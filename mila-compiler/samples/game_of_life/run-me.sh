#!/bin/bash

# This is just a wrapper around mila script for easier compilation
# and running examples in 'samples/game_of_life/'

usage () {
  echo "usage: ./run-me.sh input.txt" >&2
  echo "       where 'input.txt' is in: samples/game_of_life/input.txt" >&2
  echo "       (for example './run-me.sh basic_example.txt')" >&2
}

# Go to the root of the project
if [[ "$PWD" = */mila-compiler/samples/game_of_life ]] ; then
  cd ../..
else
  echo "FATAL: run this script from the 'mila-compiler/game_of_life/' directory" >&2
  exit 1
fi


if [ -z "$1" ] ; then
  echo "FATAL: incorrect usage" >&2
  usage
  exit 2
fi

if [ ! -f samples/game_of_life/"$1" ] ; then
  echo "FATAL: the file '$1' does not exists" >&2
  echo "       Run this script from the 'mila-compiler/game_of_life/' directory" >&2
  usage
  exit 3
fi


./mila samples/game_of_life/game_of_life.mila &&
cat samples/game_of_life/"$1" | ./compiled-mila
