use std::collections::HashMap;
use std::iter::zip;
use std::mem::swap;
use std::ops::Deref;

use inkwell::{AddressSpace, IntPredicate};
use inkwell::basic_block::BasicBlock;
use inkwell::builder::Builder;
use inkwell::context::Context;
use inkwell::module::{Linkage, Module};
use inkwell::types::BasicMetadataTypeEnum;
use inkwell::values::{BasicMetadataValueEnum, FunctionValue, IntValue, PointerValue};

use crate::ast::{Const, Expr, FunctionHeader, FunctionInfo, ProgramInfo, ScopeBlock, Variable, VariableType};
use crate::codegen::scope_variable_stack::{CodeVariable, ScopeStack, VarOrConstInCode};
use crate::codegen::semantic_error::SemanticError;
use crate::codegen::semantic_error::SemanticError::FunctionBodyDeclaredMultipleTimes;
use crate::codegen::warning::Warning;

mod semantic_error;
mod scope_variable_stack;
mod compile_expression;
mod compile_statement;
mod warning;

struct BuiltInFunctions<'a> {
    write_int: FunctionValue<'a>,
    write_string: FunctionValue<'a>,
    write_newline: FunctionValue<'a>,
    read_int: FunctionValue<'a>,
    wait_for_enter: FunctionValue<'a>,
    abort: FunctionValue<'a>,
    array_bounds_check: FunctionValue<'a>,
}

const MAIN_FUNCTION_NAME: &str = "main";

struct LlvmFunction<'ctx> {
    ptr: FunctionValue<'ctx>,
    return_var_name: Option<String>,
}

struct CompiledFunction<'ctx> {
    header: FunctionHeader,
    ptr: FunctionValue<'ctx>,
    has_compiled_body: bool,
}

// This enables to call methods/access members of `FunctionHeader` directly from `CompiledFunction`
// Meaning: `compiled_function.header.stuff()` can be just `compiled_function.stuff()`
impl<'ctx> Deref for CompiledFunction<'ctx> {
    type Target = FunctionHeader;
    fn deref(&self) -> &Self::Target {
        &self.header
    }
}

pub struct CodeGen<'ctx> {
    pub context: &'ctx Context,
    pub module: Module<'ctx>,
    pub builder: Builder<'ctx>,

    parsed_program: ProgramInfo,
    scope_stack: ScopeStack<'ctx>,
    built_in_functions: BuiltInFunctions<'ctx>,

    current_function: Option<LlvmFunction<'ctx>>,
    current_continue_destination: Option<BasicBlock<'ctx>>,
    current_break_destination: Option<BasicBlock<'ctx>>,

    declared_functions: HashMap<String, CompiledFunction<'ctx>>,

    warnings: Vec<Warning>,
}

impl<'ctx> CodeGen<'ctx> {
    pub fn new(mut program_info: ProgramInfo, context: &'ctx Context) -> Self {
        let (module, built_in_functions) = CodeGen::basic_setup(&program_info, context);
        let builder = context.create_builder();

        let mut entry_point = ScopeBlock::new();
        swap(&mut entry_point, &mut program_info.entry_point);
        program_info.functions.push(
            FunctionInfo {
                function_header: FunctionHeader {
                    function_name: MAIN_FUNCTION_NAME.to_owned(),
                    args: vec![],
                    is_function: true,
                },
                body: Some(entry_point),
            }
        );

        Self {
            context,
            module,
            builder,
            parsed_program: program_info,
            scope_stack: ScopeStack::new(),
            built_in_functions,
            current_function: None,
            current_continue_destination: None,
            current_break_destination: None,
            declared_functions: HashMap::new(),
            warnings: Vec::new(),
        }
    }

    fn basic_setup(program_info: &ProgramInfo, context: &'ctx Context) -> (Module<'ctx>, BuiltInFunctions<'ctx>) {
        let module = context.create_module(program_info.name.as_str());

        let integer_type = context.i64_type().into();
        let pointer_type = context.i64_type().ptr_type(AddressSpace::default()).into();
        let string_type = context.i8_type().ptr_type(AddressSpace::default()).into();


        let functions = [
            ("write_int", context.void_type().fn_type(&[integer_type], false)),
            ("write_string", context.void_type().fn_type(&[string_type], false)),
            ("read_int", context.i64_type().fn_type(&[pointer_type], false)),
            ("wait_for_enter", context.i64_type().fn_type(&[], false)),
            ("write_newline", context.void_type().fn_type(&[], false)),
            ("abort_program", context.void_type().fn_type(&[integer_type], false)),
            ("ARRAY_ACCESS_BOUNDS_CHECK", context.void_type().fn_type(&[integer_type, integer_type, integer_type], false)),
        ].into_iter().map(|(name, function_type)| {
            module.add_function(name, function_type, Some(Linkage::External))
        }).collect::<Vec<_>>();

        let write_int = functions[0];
        let write_string = functions[1];
        let read_int = functions[2];
        let wait_for_enter = functions[3];
        let write_newline = functions[4];
        let abort = functions[5];
        let array_bounds_check = functions[6];

        (module, BuiltInFunctions::<'ctx> {
            write_int,
            write_string,
            read_int,
            write_newline,
            wait_for_enter,
            abort,
            array_bounds_check,
        })
    }

    pub fn generate_code(&mut self) -> Result<(String, Vec<Warning>), SemanticError> {
        self.declare_global_variables()?;
        self.compile_function_definitions()?; // this also involves the implicit 'main' function
        let mut warnings = Vec::new();
        swap(&mut warnings, &mut self.warnings);

        Ok((self.module.print_to_string().to_string(), warnings))
    }


    fn cast_bool_to_int<'b>(&mut self, boolean: IntValue<'b>) -> IntValue<'b>
        where 'ctx : 'b
    {
        self.builder.build_int_cast_sign_flag(boolean, self.context.i64_type(), false, "cast_to_int").unwrap()
    }

    fn cast_int_to_bool<'b>(&mut self, int: IntValue<'b>) -> IntValue<'b>
        where 'ctx : 'b
    {
        self.builder.build_int_compare(
            IntPredicate::NE,
            int,
            self.context.i64_type().const_zero(),
            "cast_to_bool",
        ).unwrap()
    }

    fn declare_global_variables(&mut self) -> Result<(), SemanticError> {
        for Variable { variable_type, identifier } in &self.parsed_program.global_variables {
            let var_ptr = match *variable_type {
                VariableType::Integer => {
                    let ptr = self.module
                        .add_global(self.context.i64_type(), None, identifier);
                    ptr.set_initializer(&self.context.i64_type().const_zero());

                    ptr.as_pointer_value()
                }
                VariableType::Array { from, to } => {
                    let len = get_length_of_array_type(from, to, identifier)? as u32;
                    let ptr = self.module
                        .add_global(self.context.i64_type().array_type(len), None, identifier);
                    ptr.set_initializer(&self.context.i64_type().array_type(len).const_zero());

                    ptr.as_pointer_value()
                }
            };
            let var_in_code = CodeVariable { variable_type: *variable_type, llvm_ptr: var_ptr };
            self.scope_stack.add_var_to_current_scope(identifier, var_in_code)?;
        }

        for Const { identifier, value } in &self.parsed_program.global_consts {
            self.scope_stack.add_const_to_current_scope(identifier, *value)?;
        }

        Ok(())
    }

    fn compile_function_definitions(&mut self) -> Result<(), SemanticError> {
        let mut functions = Vec::new();
        swap(&mut functions, &mut self.parsed_program.functions);

        for FunctionInfo { function_header: header, body } in functions {
            match self.declared_functions.get(&header.function_name) {

                // The function has not been declared yet
                None => {
                    let llvm_args = header.args
                        .iter()
                        .map(|arg|
                            self.my_type_into_function_param_llvm_type(&arg.variable_type, &arg.identifier)
                        )
                        .collect::<Result<Vec<BasicMetadataTypeEnum>, SemanticError>>()?;

                    let ptr = self.module.add_function(
                        &header.function_name,
                        if header.is_function {
                            self.context.i64_type().fn_type(llvm_args.as_slice(), false)
                        } else {
                            self.context.void_type().fn_type(llvm_args.as_slice(), false)
                        },
                        Some(Linkage::External),
                    );

                    if self.declared_functions.insert(
                        header.function_name.clone(),
                        CompiledFunction { header: header.clone(), ptr, has_compiled_body: body.is_some() },
                    ).is_some() {
                        return Err(SemanticError::FunctionDeclaredMultipleTimes(header.function_name.clone()));
                    }

                    if let Some(body) = body {
                        self.compile_function_body(&header, ptr, body)?;
                    }
                }

                // The function has been forward declared and we have its declaration
                Some(function) => {
                    if function.has_compiled_body {
                        return Err(FunctionBodyDeclaredMultipleTimes(function.function_name.clone()));
                    }
                    let body = match body {
                        Some(function_entry_point) => function_entry_point,
                        None => return Err(SemanticError::FunctionDeclaredMultipleTimes(function.function_name.clone())),
                    };

                    self.compile_function_body(&function.header.clone(), function.ptr, body)?;
                }
            }
        }

        Ok(())
    }

    fn compile_function_body(&mut self, header: &FunctionHeader, function_ptr: FunctionValue<'ctx>, mut body: ScopeBlock) -> Result<(), SemanticError> {
        self.scope_stack.push_new_scope();

        let return_var_name = if header.is_function { Some(header.function_name.clone()) } else { None };

        self.current_function = Some(LlvmFunction { ptr: function_ptr, return_var_name });

        let entry_point_bb = self.context.append_basic_block(function_ptr, "function_entry_point");
        self.builder.position_at_end(entry_point_bb);

        for (arg, param) in zip(&header.args, function_ptr.get_params()) {
            let arg_ptr = self.create_alloca_for_variable(arg)?;
            self.builder.build_store(arg_ptr, param).unwrap();
            self.scope_stack.add_var_to_current_scope(&arg.identifier, CodeVariable {
                variable_type: arg.variable_type,
                llvm_ptr: arg_ptr,
            })?;
        }

        if header.is_function {
            body.local_variables.push(Variable::new_integer(header.function_name.clone()))
        }

        self.compile_scope_block_without_new_stack(&body)?;
        self.compile_return()?;

        self.current_function = None;
        self.scope_stack.pop_scope();
        Ok(())
    }

    fn my_type_into_function_param_llvm_type(&self, variable_type: &VariableType, identifier: &String) -> Result<BasicMetadataTypeEnum<'ctx>, SemanticError> {
        Ok(match variable_type {
            VariableType::Integer => self.context.i64_type().into(),

            VariableType::Array { from, to } => {
                let len = get_length_of_array_type(*from, *to, identifier)? as u32;
                self.context.i64_type().array_type(len).into()
            }
        })
    }

    fn declare_local_variables(&mut self, new_variables: &[Variable]) -> Result<(), SemanticError> {
        let old_builder_position = self.set_position_of_builder_to_beginning_of_current_function();

        for var in new_variables {
            let ptr = self.create_alloca_for_variable(var)?;

            self.scope_stack.add_var_to_current_scope(
                &var.identifier,
                CodeVariable {
                    variable_type: var.variable_type,
                    llvm_ptr: ptr,
                },
            )?;
        }

        self.builder.position_at_end(old_builder_position);

        Ok(())
    }

    fn create_alloca_for_variable(&mut self, variable: &Variable) -> Result<PointerValue<'ctx>, SemanticError> {
        let Variable { variable_type, identifier } = variable;

        let ptr = match *variable_type
        {
            VariableType::Array { from, to } => {
                let len = get_length_of_array_type(from, to, identifier)?;
                let ptr = self.builder.build_alloca(
                    self.context.i64_type().array_type(len as u32), identifier,
                ).unwrap();

                // set default value to array of 0
                self.builder.build_store(ptr, self.context.i64_type().array_type(len as u32).const_zero()).unwrap();

                ptr
            }
            VariableType::Integer => {
                let ptr = self.builder.build_alloca(self.context.i64_type(), identifier).unwrap();
                // set default value to 0
                self.builder.build_store(ptr, self.context.i64_type().const_zero()).unwrap();
                ptr
            }
        };

        Ok(ptr)
    }

    /// returns the previous builder position
    fn set_position_of_builder_to_beginning_of_current_function(&mut self) -> BasicBlock<'ctx> {
        let old_builder_pos = self.builder.get_insert_block().unwrap();
        let first_bb_of_function = self.get_current_llvm_function()
            .get_first_basic_block().unwrap();

        match first_bb_of_function.get_first_instruction() {
            None => self.builder.position_at_end(first_bb_of_function),
            Some(first_instruction) => self.builder.position_before(&first_instruction),
        };

        old_builder_pos
    }

    fn declare_local_consts(&mut self, new_constants: &[Const]) -> Result<(), SemanticError> {
        for Const { identifier, value } in new_constants {
            self.scope_stack.add_const_to_current_scope(identifier, *value)?;
        }

        Ok(())
    }

    fn get_variable(&self, identifier: &str) -> Result<&CodeVariable<'ctx>, SemanticError> {
        self.scope_stack.get_variable(identifier)
            .ok_or_else(|| SemanticError::UndeclaredIdentifier { identifier: identifier.to_owned() })
    }
    fn get_integer_variable(&self, identifier: &str) -> Result<&PointerValue<'ctx>, SemanticError> {
        let var = self.get_variable(identifier)?;
        let CodeVariable { variable_type, llvm_ptr } = var;
        match variable_type {
            VariableType::Integer => Ok(llvm_ptr),
            VariableType::Array { .. } => Err(SemanticError::UsingArrayAsInteger(identifier.to_owned())),
        }
    }

    fn get_symbol(&self, identifier: &str) -> Result<VarOrConstInCode<'ctx>, SemanticError> {
        self.scope_stack.get_value(identifier)
            .ok_or_else(|| SemanticError::UndeclaredIdentifier { identifier: identifier.to_owned() })
    }

    fn get_array_variable(&self, identifier: &str) -> Result<(PointerValue<'ctx>, i64, i64), SemanticError> {
        if let VarOrConstInCode::Var(
            CodeVariable {
                variable_type: VariableType::Array { from, to },
                llvm_ptr
            }
        ) = self.get_symbol(identifier)? {
            return Ok((llvm_ptr, from, to));
        }

        Err(SemanticError::UsingArrayAsInteger(identifier.to_owned()))
    }

    fn get_current_llvm_function(&self) -> FunctionValue<'ctx> {
        return self.current_function
            .as_ref()
            .expect(
                "this method should never be called when we are not in a function"
            ).ptr
            .clone();
    }

    fn get_function_declaration(&self, function_identifier: &str) -> Result<&CompiledFunction<'ctx>, SemanticError> {
        self.declared_functions.get(function_identifier).ok_or_else(
            || SemanticError::CouldNotFindFunctionWithName(function_identifier.to_owned())
        )
    }

    fn translate_expressions_into_llvm_args(&mut self, expressions: &[Expr], header: &FunctionHeader) -> Result<Vec<BasicMetadataValueEnum<'ctx>>, SemanticError> {
        if expressions.len() != header.args.len() {
            return Err(SemanticError::IncorrectNumberOfArguments {
                function_id: header.function_name.clone(),
                expected: header.args.len(),
                got: expressions.len(),
            });
        }

        let mut llvm_args = Vec::new();

        for (arg, Variable { variable_type, identifier }) in zip(expressions, &header.args) {
            match variable_type {
                VariableType::Integer => {
                    llvm_args.push(self.compile_expression(arg)?.into());
                }
                VariableType::Array { from, to } => {
                    let array_id =
                        if let Expr::Var(array_id) = arg { array_id } else {
                            return Err(SemanticError::InvalidArgument { function_id: header.function_name.clone() });
                        };

                    let (ptr, actual_from, actual_to) = self.get_array_variable(array_id)?;
                    let expected_len = get_length_of_array_type(*from, *to, identifier)?;
                    let actual_len = get_length_of_array_type(actual_from, actual_to, array_id)?;
                    if expected_len > actual_len {
                        return Err(SemanticError::CastToArrayWithTooBigBounds {
                            id: array_id.clone(),
                            from1: actual_from,
                            to1: actual_to,
                            from2: *from,
                            to2: *to,
                        });
                    }
                    let array = self.builder.build_load(ptr, "").unwrap();
                    llvm_args.push(array.into());
                }
            }
        }

        Ok(llvm_args)
    }

    fn compile_return(&mut self) -> Result<(), SemanticError> {
        match &self.current_function.as_ref()
            .expect("Return statement should not be called outside of a function")
            .return_var_name
        {
            Some(return_value_name) => {
                let return_value = self.compile_expression(&Expr::Var(return_value_name.clone()))?;
                self.builder.build_return(Some(&return_value)).unwrap();
            }
            None => {
                self.builder.build_return(None).unwrap();
            }
        };
        Ok(())
    }
}

fn get_length_of_array_type(from: i64, to: i64, identifier: &str) -> Result<u64, SemanticError> {
    if from > to {
        return Err(SemanticError::ArrayHasFlippedBounds { array_identifier: identifier.to_owned(), from, to });
    }

    let len = from.abs_diff(to) + 1;
    Ok(len)
}

