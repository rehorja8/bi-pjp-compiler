use std::collections::HashMap;

use inkwell::values::PointerValue;

use crate::ast::VariableType;
use crate::codegen::semantic_error::SemanticError;

pub struct ScopeStack<'ctx> {
    stack: Vec<VariableScope<'ctx>>,
}

impl<'ctx> ScopeStack<'ctx> {
    pub fn new() -> Self {
        Self { stack: vec![VariableScope::new()] }
    }

    pub fn push_new_scope(&mut self) {
        self.stack.push(VariableScope::new())
    }

    pub fn pop_scope(&mut self) {
        // We do not want to pop the global space
        if self.stack.len() >= 2 {
            self.stack.pop();
        }
    }

    pub fn add_var_to_current_scope(&mut self, identifier: &str, code_variable: CodeVariable<'ctx>) -> Result<(), SemanticError> {
        let identifier = identifier.to_owned();
        let current_scope = self.current_scope_mut();

        if current_scope.consts.contains_key(&identifier) {
            return Err(SemanticError::VariableRedefinition { identifier, collision_with_const: true });
        }
        if current_scope.vars.contains_key(&identifier) {
            return Err(SemanticError::VariableRedefinition { identifier, collision_with_const: false });
        }

        current_scope.vars.insert(identifier, code_variable);
        Ok(())
    }

    pub fn add_const_to_current_scope(&mut self, identifier: &str, value: i64) -> Result<(), SemanticError> {
        let current_scope = self.current_scope_mut();

        if current_scope.consts.contains_key(identifier) {
            return Err(SemanticError::ConstRedefinition { identifier: identifier.to_owned(), collision_with_var: false });
        }
        if current_scope.vars.contains_key(identifier) {
            return Err(SemanticError::ConstRedefinition { identifier: identifier.to_owned(), collision_with_var: true });
        }

        current_scope.consts.insert(identifier.to_owned(), value);
        Ok(())
    }

    pub fn get_variable(&self, identifier: &str) -> Option<&CodeVariable<'ctx>> {
        for scope in self.stack.iter().rev() {
            let var = scope.vars.get(identifier);
            if var.is_some() { return var; };
        }

        None
    }

    pub fn get_value(&self, identifier: &str) -> Option<VarOrConstInCode<'ctx>> {
        for scope in self.stack.iter().rev() {
            if let Some(var) = scope.vars.get(identifier) {
                return Some(VarOrConstInCode::Var(*var));
            }
            if let Some(const_value) = scope.consts.get(identifier) {
                return Some(VarOrConstInCode::Const(*const_value));
            }
        }

        None
    }

    fn current_scope_mut(&mut self) -> &mut VariableScope<'ctx> {
        self.stack.last_mut().expect("the var stack should never be empty")
    }
}

pub enum VarOrConstInCode<'ctx> {
    Var(CodeVariable<'ctx>),
    Const(i64),
}

pub struct VariableScope<'ctx> {
    vars: HashMap<String, CodeVariable<'ctx>>,
    consts: HashMap<String, i64>,
}

impl<'ctx> VariableScope<'ctx> {
    fn new() -> Self {
        Self { vars: HashMap::new(), consts: HashMap::new() }
    }
}

#[derive(Copy, Clone)]
pub struct CodeVariable<'ctx> {
    pub variable_type: VariableType,
    pub llvm_ptr: PointerValue<'ctx>,
}
