use inkwell::basic_block::BasicBlock;
use inkwell::builder::Builder;
use inkwell::IntPredicate;

use crate::ast::{BinaryCmpType, BinaryOpType, Expr, ForLoop, ForLoopDirection, Printable, ScopeBlock, Statement};
use crate::codegen::CodeGen;
use crate::codegen::semantic_error::SemanticError;
use crate::codegen::warning::{Severity, Warning};

impl<'ctx> CodeGen<'ctx> {
    pub(super) fn compile_statement(&mut self, statement: &Statement) -> Result<(), SemanticError> {
        match statement {
            Statement::Block(scope_block) => self.compile_scope_block(scope_block),
            Statement::Assignment(lhs, rhs) => {
                let lhs = self.compile_assignable(lhs)?;
                let rhs = self.compile_expression(rhs)?;
                self.builder.build_store(lhs, rhs).unwrap();

                Ok(())
            }
            Statement::Abort(exit_code_exp) => {
                let exit_code = self.compile_expression(exit_code_exp)?.into();
                self.builder.build_call(self.built_in_functions.abort, &[exit_code], "").unwrap();
                Ok(())
            }
            Statement::JustExpression(expression) => {
                // This is my way of dealing with procedures and functions...
                if let Expr::FunctionCall { func_id, args } = expression {
                    let function = self.get_function_declaration(func_id)?;
                    let function_ptr = function.ptr;

                    if !function.is_function {
                        let llvm_args = self.translate_expressions_into_llvm_args(
                            args.as_slice(),
                            &function.header.clone(), // the .clone() is sad but i don't know a non-ugly way of doing this
                        )?;
                        self.builder.build_call(function_ptr, llvm_args.as_slice(), "call-to_function").unwrap();
                        return Ok(());
                    }
                }

                self.log_unused_expression(expression);
                self.compile_expression(expression).map(|_| ())
            }
            Statement::Write(to_print) => {
                self.compile_write_statement(&to_print)?;
                Ok(())
            }
            Statement::WriteLn(to_print) => {
                self.compile_statement(&Statement::Write(to_print.clone()))?;
                self.builder.build_call(
                    self.built_in_functions.write_newline, &[], "print_newline",
                ).unwrap();

                Ok(())
            }
            Statement::If { cond, true_branch, else_branch } => {
                self.compile_if_statement(cond, true_branch, else_branch
                    .as_ref()
                    .map(|stm_ref| stm_ref.as_ref()),
                )
            }
            Statement::While { cond, body } => {
                self.compile_while_loop(cond, body)
            }
            Statement::For(for_loop_details) => {
                self.compile_for_loop(for_loop_details)
            }
            Statement::Inc(assignable, by) => {
                let variable_ptr = self.compile_assignable(assignable)?;
                let old_value = self.builder.build_load(variable_ptr, "old_value").unwrap();
                let builder_method = if by.is_negative() {
                    Builder::build_int_sub
                } else {
                    Builder::build_int_add
                };
                let new_value = builder_method(
                    &self.builder,
                    old_value.into_int_value(),
                    self.context.i64_type().const_int(by.abs() as u64, false),
                    "new_value",
                ).unwrap();

                self.builder.build_store(variable_ptr, new_value).unwrap();

                Ok(())
            }
            Statement::Exit => {
                self.compile_return()?;

                // Jump to a dead code block
                let dead_code_bb = self.context.append_basic_block(self.get_current_llvm_function(), "dead_code_block");
                self.builder.position_at_end(dead_code_bb);

                Ok(())
            }
            Statement::Break => {
                self.compile_jump_statement(self.current_break_destination, SemanticError::BreakOutsideOfLoop)
            }
            Statement::Continue => {
                self.compile_jump_statement(self.current_continue_destination, SemanticError::ContinueOutsideOfLoop)
            }
        }
    }
    fn compile_write_statement(&mut self, to_print: &[Printable]) -> Result<(), SemanticError> {
        for printable in to_print {
            match printable {
                Printable::Expression(expression) => {
                    let expression = self.compile_expression(expression)?;
                    self.builder.build_call(
                        self.built_in_functions.write_int, &[expression.into()], "call_to_write",
                    ).unwrap();
                }
                Printable::StringLiteral(string_literal) => {
                    let string_literal =
                        match string_literal.as_str() {
                            "[-CURSOR-TO-HOME-]" => "\x1b[H",
                            "[-HIDE-CURSOR-]" => "\x1b[?25l",
                            "[-SHOW-CURSOR-]" => "\x1b[?25h",
                            "[-CLEAR-THE-SCREEN-]" => "\x1b[2J",
                            _ => string_literal
                        };
                    let string_literal = self
                        .builder
                        .build_global_string_ptr(string_literal, "string_literal_to_print")
                        .unwrap()
                        .as_pointer_value()
                        .into();

                    self.builder.build_call(
                        self.built_in_functions.write_string, &[string_literal], "call_to_write",
                    ).unwrap();
                }
            };
        }
        Ok(())
    }

    fn log_unused_expression(&mut self, expression: &Expr) {
        match expression {
            Expr::ReadLn(None) => { /*do nothing*/ }
            Expr::BinOp { lhs: _, op_type: BinaryOpType::Compare(BinaryCmpType::IsEqual), rhs: _ } => {
                self.warnings.push(Warning::new(
                    Severity::Critical,
                    "Unused result of int comparison '=' - this is probably '=' meant as a ':='",
                ));
            }
            Expr::ReadLn(Some(_)) => {
                self.warnings.push(Warning::new(
                    Severity::Critical,
                    "Not-assigned result of 'readln(...)' which returns: \n\
                               0 if the value was read correctly,\n\
                               1 if incorrectly,\n\
                              -1 if eof was reached",
                ));
            }
            _ => {
                self.warnings.push(Warning::new(Severity::Small, "Unused result of expression"))
            }
        }
    }

    fn compile_jump_statement(&mut self, destination: Option<BasicBlock<'ctx>>, err: SemanticError) -> Result<(), SemanticError> {
        let break_destination = match destination {
            None => return Err(err),
            Some(break_destination) => break_destination,
        };
        self.builder.build_unconditional_branch(break_destination).unwrap();

        // Jump to a dead code block
        let dead_code_bb = self.context.append_basic_block(self.get_current_llvm_function(), "dead_code_block");
        self.builder.position_at_end(dead_code_bb);

        Ok(())
    }


    pub(super) fn compile_scope_block(&mut self, scope_block: &ScopeBlock) -> Result<(), SemanticError> {
        self.compile_scope_block_inner(scope_block, true)
    }

    pub(super) fn compile_scope_block_without_new_stack(&mut self, scope_block: &ScopeBlock) -> Result<(), SemanticError> {
        self.compile_scope_block_inner(scope_block, false)
    }

    fn compile_scope_block_inner(&mut self, scope_block: &ScopeBlock, with_new_scope_stack: bool) -> Result<(), SemanticError> {
        let ScopeBlock { local_consts, local_variables, statements } = scope_block;
        if with_new_scope_stack { self.scope_stack.push_new_scope(); }

        self.declare_local_variables(local_variables.as_slice())?;
        self.declare_local_consts(local_consts.as_slice())?;

        for statement in statements {
            self.compile_statement(statement)?;
        }

        if with_new_scope_stack { self.scope_stack.pop_scope(); }
        Ok(())
    }

    fn compile_if_statement(&mut self, condition: &Expr, true_branch_stm: &Statement, else_branch_stm: Option<&Statement>,
    ) -> Result<(), SemanticError> {
        let current_function = self.get_current_llvm_function();

        // LLVM basic blocks for control flow
        let true_branch_bb = self.context.append_basic_block(current_function, "if_true_branch_bb");
        let else_branch_bb = self.context.append_basic_block(current_function, "else_branch_bb");
        let after_branch_bb =
            match else_branch_stm {
                None => else_branch_bb,
                Some(_) => self.context.append_basic_block(current_function, "if_else_after_branch_bb"),
            };

        // If condition
        let condition = self.compile_expression(condition)?;
        let condition_result = self.cast_int_to_bool(condition);
        self.builder.build_conditional_branch(condition_result, true_branch_bb, else_branch_bb).unwrap();

        // True branch
        self.builder.position_at_end(true_branch_bb);
        self.compile_statement(true_branch_stm)?;
        self.builder.build_unconditional_branch(after_branch_bb).unwrap();

        // Else branch
        if let Some(else_branch_stm) = else_branch_stm {
            self.builder.position_at_end(else_branch_bb);
            self.compile_statement(else_branch_stm)?;
            self.builder.build_unconditional_branch(after_branch_bb).unwrap();
        }

        // After branch
        self.builder.position_at_end(after_branch_bb);

        Ok(())
    }

    fn compile_while_loop(&mut self, condition: &Expr, body: &Statement) -> Result<(), SemanticError> {
        let current_function = self.get_current_llvm_function();
        let condition_bb = self.context.append_basic_block(current_function, "condition_bb");
        let while_loop_body_bb = self.context.append_basic_block(current_function, "while_loop_body_bb");
        let after_while_loop_bb = self.context.append_basic_block(current_function, "after_while_loop_bb");

        // Update break/continue options
        self.current_break_destination = Some(after_while_loop_bb);
        self.current_continue_destination = Some(condition_bb);

        // Set builder to the condition basic block and compile the condition code
        self.builder.build_unconditional_branch(condition_bb).unwrap();
        self.builder.position_at_end(condition_bb);
        //
        let condition = self.compile_expression(condition)?;
        let condition_result = self.cast_int_to_bool(condition);
        //
        self.builder.build_conditional_branch(condition_result, while_loop_body_bb, after_while_loop_bb).unwrap();

        // Compile the while loop body
        self.builder.position_at_end(while_loop_body_bb);
        self.compile_statement(body)?;
        self.builder.build_unconditional_branch(condition_bb).unwrap();

        // Update break/continue options
        self.current_break_destination = None;
        self.current_continue_destination = None;

        // Set position of the builder to the after basic block
        self.builder.position_at_end(after_while_loop_bb);

        Ok(())
    }

    fn compile_for_loop(&mut self, loop_details: &ForLoop) -> Result<(), SemanticError> {

        // Initial assignment setup of iteration variable
        let iteration_var = self.compile_assignable(&loop_details.var)?;
        let initial_value = self.compile_expression(&loop_details.from)?;
        self.builder.build_store(iteration_var, initial_value).unwrap();

        // LLVM basic blocks for control flow
        let current_function = self.get_current_llvm_function();
        let loop_condition_bb = self.context.append_basic_block(current_function, "for_loop_condition_bb");
        let loop_body_bb = self.context.append_basic_block(current_function, "for_loop_body_bb");
        let increment_bb = self.context.append_basic_block(current_function, "for_loop_increment_bb");
        let after_loop_bb = self.context.append_basic_block(current_function, "after_for_loop_bb");

        // Update break/continue options
        self.current_break_destination = Some(after_loop_bb);
        self.current_continue_destination = Some(increment_bb);

        // Jump to the loop condition
        self.builder.build_unconditional_branch(loop_condition_bb).unwrap();

        // Condition
        self.builder.position_at_end(loop_condition_bb);
        let iteration_var_value = self.builder.build_load(iteration_var, "current_iterator_value")
            .unwrap()
            .into_int_value();
        let upper_bound = self.compile_expression(&loop_details.to).unwrap();
        let compare_relation = match loop_details.direction {
            ForLoopDirection::To => IntPredicate::SLE,
            ForLoopDirection::DownTo => IntPredicate::SGE,
        };
        let comparison_result = self.builder.build_int_compare(
            compare_relation, iteration_var_value, upper_bound, "for_loop_comparison",
        ).unwrap();
        self.builder.build_conditional_branch(comparison_result, loop_body_bb, after_loop_bb).unwrap();


        // Body
        self.builder.position_at_end(loop_body_bb);
        self.compile_statement(&loop_details.body)?;
        self.builder.build_unconditional_branch(increment_bb).unwrap();

        // Decrement/Increment
        self.builder.position_at_end(increment_bb);
        let increment: i64 = match loop_details.direction {
            ForLoopDirection::To => 1,
            ForLoopDirection::DownTo => -1,
        };
        self.compile_statement(
            &Statement::Inc(
                loop_details.var.clone(),
                increment,
            )
        )?;
        self.builder.build_unconditional_branch(loop_condition_bb).unwrap();

        // Update break/continue options
        self.current_break_destination = None;
        self.current_continue_destination = None;

        // After block
        self.builder.position_at_end(after_loop_bb);

        Ok(())
    }
}