use crate::terminal_colors::*;

pub struct Warning {
    severity: Severity,
    message: String,
}

#[derive(PartialEq)]
pub enum Severity {
    Small,
    Critical,
}

impl Warning {
    pub fn new(severity: Severity, message: &str) -> Self {
        Self {
            severity,
            message: message.to_owned(),
        }
    }

    pub fn print(&self) {
        let color = match self.severity {
            Severity::Small => { YELLOW }
            Severity::Critical => { RED }
        };
        eprintln!(
            "{BOLD}{color}{}WARNING: {CLEAR}{}",
            if self.severity == Severity::Critical { "SERIOUS-" } else { "" },
            self.message
        );
    }
}