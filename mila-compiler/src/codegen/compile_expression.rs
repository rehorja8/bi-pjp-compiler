use inkwell::builder::Builder;
use inkwell::IntPredicate;
use inkwell::values::{BasicValueEnum, IntValue, PointerValue};

use crate::ast::{ArithmeticBinaryOpType, Assignable, BinaryCmpType, BinaryOpType, Expr, LogicalBinaryOpType, UnaryOpType, VariableType};
use crate::codegen::scope_variable_stack::{CodeVariable, VarOrConstInCode};
use crate::codegen::semantic_error::{INTERNAL_EXPRESSION_SHOULD_NOT_FAIL, SemanticError};

impl<'ctx> super::CodeGen<'ctx> {
    pub(super) fn compile_assignable(&mut self, assignable: &Assignable) -> Result<PointerValue<'ctx>, SemanticError> {
        match assignable {
            Assignable::Var(identifier) => {
                Ok(self.get_integer_variable(&identifier)?.clone())
            }
            Assignable::ArrayAccess { array_id, index } => {
                self.compile_array_access(array_id, index)
            }
        }
    }

    pub(super) fn compile_expression(&mut self, expression: &Expr) -> Result<IntValue<'ctx>, SemanticError> {
        match expression {
            Expr::UnaryOp { op_type, arg } => {
                self.compile_unary_operation(arg, op_type)
            }
            Expr::BinOp { lhs, op_type, rhs } => {
                let lhs = self.compile_expression(lhs)?;
                let rhs = self.compile_expression(rhs)?;

                Ok(match op_type {
                    BinaryOpType::Arithmetic(operation) => {
                        self.compile_arithmetic_expression(lhs, rhs, operation)
                    }
                    BinaryOpType::Compare(relation) => {
                        self.compile_compare_expression(lhs, rhs, relation)
                    }
                    BinaryOpType::Logical(operation) => {
                        self.compile_logical_operation(lhs, rhs, operation)
                    }
                })
            }
            Expr::FunctionCall { func_id, args } => {
                let function = self.get_function_declaration(func_id)?;
                let function_ptr = function.ptr;
                if !function.is_function {
                    return Err(SemanticError::UsingProcedureAsFunction(func_id.to_string()));
                }

                let llvm_args = self.translate_expressions_into_llvm_args(
                    args.as_slice(),
                    &function.header.clone(), // the .clone() is sad but i don't know a non-ugly way of doing this
                )?;

                let return_value = self.builder.build_call(function_ptr, llvm_args.as_slice(), "call-to_function")
                    .unwrap()
                    .try_as_basic_value()
                    .left()
                    .unwrap()
                    .into_int_value();

                Ok(return_value)
            }
            Expr::ArrayAccess { array_id, index } => {
                let ptr = self.compile_array_access(&array_id, index)?;
                Ok(self.builder.build_load(ptr, "array_access_value").unwrap().into_int_value())
            }
            Expr::Var(identifier) => {
                match self.get_symbol(&identifier)? {
                    VarOrConstInCode::Var(CodeVariable { variable_type, llvm_ptr }) => {
                        if variable_type != VariableType::Integer { return Err(SemanticError::UsingArrayAsInteger(identifier.clone())); }
                        let loaded_variable: BasicValueEnum = self.builder.build_load(llvm_ptr, "").unwrap();
                        Ok(loaded_variable.into_int_value())
                    }
                    VarOrConstInCode::Const(value_of_const) => {
                        let abs_value = value_of_const.abs() as u64;
                        let compiled_abs_value = self.compile_expression(&Expr::Number(abs_value))?;
                        Ok(
                            if value_of_const.is_negative() {
                                self.builder.build_int_neg(compiled_abs_value, "").unwrap()
                            } else {
                                compiled_abs_value
                            }
                        )
                    }
                }
            }
            Expr::Number(value) => Ok(self.context.i64_type().const_int(*value, false)),
            Expr::ReadLn(var_to_read_into) => {
                let (function, params) =
                    match var_to_read_into {
                        Some(assignable) => {
                            (self.built_in_functions.read_int, vec![self.compile_assignable(assignable)?.into()])
                        }
                        None => {
                            (self.built_in_functions.wait_for_enter, vec![])
                        }
                    };

                Ok(
                    self.builder.build_call(
                        function, params.as_slice(), "call_to_readline",
                    ).unwrap().try_as_basic_value().left().unwrap().into_int_value()
                )
            }
        }
    }

    pub(super) fn compile_unary_operation(&mut self, arg: &Expr, op_type: &UnaryOpType) -> Result<IntValue<'ctx>, SemanticError> {
        let arg = self.compile_expression(arg)?;
        Ok(match op_type {
            UnaryOpType::UnaryPlus => arg,
            UnaryOpType::UnaryMinus => {
                self.builder.build_int_neg(arg, "unary_minus").unwrap()
            }
            UnaryOpType::Negation => {
                let arg = self.cast_int_to_bool(arg);
                let not_result = self.builder.build_not(arg, "unary_not").unwrap();
                self.cast_bool_to_int(not_result)
            }
        })
    }

    pub(super) fn compile_logical_operation(&mut self, lhs: IntValue<'ctx>, rhs: IntValue<'ctx>, operation: &LogicalBinaryOpType) -> IntValue<'ctx> {
        let lhs = self.cast_int_to_bool(lhs);
        let rhs = self.cast_int_to_bool(rhs);
        let builder_method = match operation {
            LogicalBinaryOpType::LogicalOr => Builder::build_or,
            LogicalBinaryOpType::LogicalAnd => Builder::build_and,
        };

        let operation_result = builder_method(&self.builder, lhs, rhs, "logical_operation").unwrap();
        self.cast_bool_to_int(operation_result)
    }

    pub(super) fn compile_compare_expression(&mut self, lhs: IntValue<'ctx>, rhs: IntValue<'ctx>, relation: &BinaryCmpType) -> IntValue<'ctx> {
        let llvm_int_relation = match relation {
            BinaryCmpType::IsEqual => IntPredicate::EQ,
            BinaryCmpType::IsNotEqual => IntPredicate::NE,
            BinaryCmpType::Less => IntPredicate::SLT,
            BinaryCmpType::Greater => IntPredicate::SGT,
            BinaryCmpType::LessOrEqual => IntPredicate::SLE,
            BinaryCmpType::GreaterOrEqual => IntPredicate::SGE,
        };

        let cmp_result = self.builder.build_int_compare(llvm_int_relation, lhs, rhs, "int_compare").unwrap();
        self.cast_bool_to_int(cmp_result)
    }

    pub(super) fn compile_arithmetic_expression(&mut self, lhs: IntValue<'ctx>, rhs: IntValue<'ctx>, operation: &ArithmeticBinaryOpType) -> IntValue<'ctx> {
        let builder_method = match operation {
            ArithmeticBinaryOpType::Plus => Builder::build_int_add,
            ArithmeticBinaryOpType::Minus => Builder::build_int_sub,
            ArithmeticBinaryOpType::Multiply => Builder::build_int_mul,
            ArithmeticBinaryOpType::Divide => Builder::build_int_signed_div,
            ArithmeticBinaryOpType::Modulo => Builder::build_int_signed_rem,
        };
        builder_method(&self.builder, lhs, rhs, "arithmetic_op").unwrap()
    }

    pub(super) fn compile_array_access(&mut self, array_identifier: &str, raw_index: &Expr) -> Result<PointerValue<'ctx>, SemanticError> {
        let raw_index = self.compile_expression(raw_index)?;

        let symbol = self.get_symbol(array_identifier)?;
        let (from, to, llvm_ptr) = match symbol {
            VarOrConstInCode::Var(
                CodeVariable {
                    variable_type: VariableType::Array { from, to },
                    llvm_ptr
                }) => (from, to, llvm_ptr),
            _ => return Err(SemanticError::UsingIntegerAsArray(array_identifier.to_owned())),
        };
        
        let from = self.compile_signed_number(from);
        let to = self.compile_signed_number(to);
        
        self.builder.build_call(self.built_in_functions.array_bounds_check, &[
            from.into(), to.into(), raw_index.into()
        ], "").unwrap();
        
        let corrected_index = self.builder.build_int_sub(raw_index, from, "corrected_index").unwrap();

        unsafe {
            Ok(self.builder.build_gep(
                llvm_ptr,
                &[self.context.i64_type().const_zero(), corrected_index],
                "array_access_pointer",
            ).unwrap())
        }
    }

    fn compile_signed_number(&mut self, number: i64) -> IntValue<'ctx> {
        self.compile_unary_operation(
            &Expr::Number(number.abs() as u64),
            match number.signum() {
                -1 => &UnaryOpType::UnaryMinus,
                _ => &UnaryOpType::UnaryPlus,
            },
        ).expect(INTERNAL_EXPRESSION_SHOULD_NOT_FAIL)
    }
}
