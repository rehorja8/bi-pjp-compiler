#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

void write_int(int64_t val) {
    printf("%ld", val);
    fflush(stdout);
}

void write_string(char* str) {
    printf("%s", str);
    fflush(stdout);
}

void write_newline() {
    printf("\n");
    fflush(stdout);
}

/// @return -  `0` on success
///         -  `1` on failure of read (the value of `destination` does not get updated)
///         - `-1` on eof             (the value of `destination` does not get updated)
int64_t read_int(int64_t* destination) {
    switch (scanf(" %ld", destination)) {
        case 1:
            return 0;
        case EOF:
            return -1;
        default:
            getchar();
            return 1;
    }
}

/// @return - `-1` on eof
///         -  `0` otherwise
int64_t wait_for_enter() {
    getchar();
    while (1) {
        int64_t result = getchar();
        if (result == EOF) return -1;
        else if (result == '\n') return 0;
    }
}

void ARRAY_ACCESS_BOUNDS_CHECK(int64_t from, int64_t to, int64_t variable) {
    if (!(from <= variable && variable <= to)) {
        fprintf(stderr,
                "\x1b[1;31mRUNTIME-ERROR:\x1b[0m\x1b[31m trying to access "
                "the array [%ld..%ld] at index '%ld'\n",
                from, to, variable
        );
        exit(1);
    }
}

void abort_program(int64_t code) {
    exit(code);
}