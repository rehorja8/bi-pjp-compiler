pub const RED: &'static str = "\x1b[31m";
pub const YELLOW: &'static str = "\x1b[33m";
pub const BOLD: &'static str = "\x1b[31;1m";
pub const UNDERLINE: &'static str = "\x1b[4m";
pub const CLEAR: &'static str = "\x1b[0m";
