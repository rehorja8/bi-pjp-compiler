use std::{collections::HashMap, fmt};
use std::fmt::{Debug, Display};
use std::fmt::Formatter;

use strum_macros::Display;

#[derive(Debug, Clone, Copy, PartialEq, Eq, Display)]
pub enum KeywordType {
    Program,
    Var,
    Const,
    Integer,
    Function,
    Procedure,
    Forward,
    Begin,
    End,
    ParentOpen,
    ParentClose,
    WriteLn,
    Write,
    ReadLn,
    Semicolon,
    EqualSign,
    Mult,
    Plus,
    Minus,
    LogicalNot,
    Dot,
    Comma,
    Colon,
    Assign,
    Div,
    Mod,
    Less,
    More,
    NotEq,
    LessOrEqual,
    MoreOrEqual,
    RightSquareBracket,
    LeftSquareBracket,
    Do,
    Or,
    And,
    If,
    Else,
    Exit,
    Abort,
    Break,
    Continue,
    Then,
    To,
    DownTo,
    While,
    For,
    Dec,
    Inc,
    Array,
    Of,
    DoubleDot,
}

impl KeywordType {
    fn str_pairs() -> [(&'static str, KeywordType); 32] {
        {
            use KeywordType::*;
            [
                ("program", Program),
                ("var", Var),
                ("const", Const),
                ("integer", Integer),
                ("function", Function),
                ("procedure", Procedure),
                ("forward", Forward),
                ("begin", Begin),
                ("end", End),
                ("writeln", WriteLn),
                ("write", Write),
                ("readln", ReadLn),
                ("inc", Inc),
                ("dec", Dec),
                ("div", Div),
                ("mod", Mod),
                ("do", Do),
                ("or", Or),
                ("and", And),
                ("if", If),
                ("else", Else),
                ("exit", Exit),
                ("abort", Abort),
                ("break", Break),
                ("continue", Continue),
                ("then", Then),
                ("to", To),
                ("downto", DownTo),
                ("while", While),
                ("for", For),
                ("array", Array),
                ("of", Of),
            ]
        }
    }
}

lazy_static! {
    pub static ref KEYWORD_MAP: HashMap<String, KeywordType> = HashMap::from_iter(
        KeywordType::str_pairs()
            .into_iter()
            .map(|(string, keyword)| (string.to_owned(), keyword)),
    );
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum Token {
    Keyword(KeywordType),
    IntLiteral(u64),
    StringLiteral(String),
    Identifier(String),
    Eof,
}

impl Display for Token {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match self {
            Token::Keyword(kw) => write!(f, "Keyword({kw})"),
            Token::IntLiteral(number) => write!(f, "IntLiteral({number})"),
            Token::StringLiteral(str) => write!(f, "StringLiteral({str})"),
            Token::Identifier(str) => write!(f, "Identifier({str})"),
            Token::Eof => write!(f, "Eof"),
        }
    }
}

#[derive(Default, Copy, Clone, Debug)]
pub struct CodeCoordinate {
    pub index: usize,
    pub row: usize,
    pub column: usize,
}

impl CodeCoordinate {
    pub fn new() -> Self {
        CodeCoordinate {
            index: 0,
            row: 1,
            column: 1,
        }
    }

    pub fn update_with(&mut self, passing_char: char) {
        self.index += 1;
        self.column += 1;

        if passing_char == '\n' {
            self.column = 1;
            self.row += 1;
        }
    }
}


impl fmt::Display for CodeCoordinate {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "[i:{:02}, r:{:02}, c:{:02}]", self.index, self.row, self.column)
    }
}

#[derive(Default, Copy, Clone, Debug)]
pub struct CodeLocation {
    pub start: CodeCoordinate,
    pub end: CodeCoordinate,
}

impl CodeLocation {
    pub fn new(start_position: CodeCoordinate, end_position: CodeCoordinate) -> Self {
        Self { start: start_position, end: end_position }
    }
}

#[derive(Debug, Clone)]
pub struct PositionedToken {
    pub token: Token,
    pub position: CodeLocation,
}

impl PositionedToken {
    pub fn new(token: Token, start_position: CodeCoordinate, end_position: CodeCoordinate) -> Self {
        PositionedToken {
            token,
            position: CodeLocation::new(start_position, end_position),
        }
    }
}

impl fmt::Display for PositionedToken {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "{}-{}: {:?}", self.position.start, self.position.end, self.token)
    }
}

