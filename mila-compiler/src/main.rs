#[macro_use]
extern crate lazy_static;

use std::io;
use std::io::Read;

use inkwell::context::Context;

use lexer::parse_error::PositionedParseError;

use crate::codegen::CodeGen;
use crate::lexer::Lexer;
use crate::parser::Parser;
use crate::terminal_colors::*;

mod ast;
mod codegen;
mod parser;
mod tokens;
mod lexer;
mod terminal_colors;

type Code = Vec<char>;

fn main() -> Result<(), i32> {
    let buffer = get_code_for_compilation()
        .map_err(|e| {
            eprintln!("{RED}{BOLD}FATAL:{CLEAR} Could not read the entire code: {e}");
            return 1;
        })?;

    let mut char_iterator = buffer.iter().copied();
    let lexer = Lexer::new(&mut char_iterator);
    let mut parser = Parser::from_lexer(lexer);

    let parsed_program = match parser.generate_ast() {
        Ok(ast) => ast,
        Err(e) => {
            print_parse_error(&e, &buffer);
            return Err(2);
        }
    };

    let ctx = Context::create();
    let mut code_gen = CodeGen::new(parsed_program, &ctx);

    match code_gen.generate_code() {
        Err(semantic_error) => {
            eprintln!("{RED}{BOLD}SEMANTIC-ERROR: {CLEAR}{:?}", semantic_error);
            Err(3)
        }
        Ok((generated_llvm, warnings)) => {
            warnings.iter().for_each(|warning| warning.print());
            println!("{generated_llvm}");
            Ok(())
        }
    }
}

fn get_code_for_compilation<'a>() -> io::Result<Code> {
    let mut buffer = String::new();
    io::stdin().read_to_string(&mut buffer)?;
    Ok(buffer.chars().collect())
}


fn print_parse_error(error: &PositionedParseError, code: &Code) {
    eprintln!("{RED}{BOLD}LEX/PARSE-ERROR: {CLEAR}{}", error.error.message);
    eprint!("{RED}at (r:{}|c:{})-(r:{}|c:{}):  {CLEAR}",
            error.position.start.row,
            error.position.start.column,
            error.position.end.row,
            error.position.end.column
    );
    let (line_start, line_end) = get_line_boundaries_at(
        error.position.start.index,
        error.position.end.index,
        &code,
    );

    let start_id = error.position.start.index;
    let end_id = error.position.end.index;

    if start_id >= code.len() {
        for i in line_start..code.len() { eprint!("{}", code[i]) }
        eprint!("{UNDERLINE}{RED} (error-here) {CLEAR}");
    } else {
        for i in line_start..start_id { eprint!("{}", code[i]); }
        eprint!("{UNDERLINE}{RED}");
        for i in start_id..=end_id { eprint!("{}", code[i]); }
        eprint!("{CLEAR}");
        for i in end_id + 1..=line_end { eprint!("{}", code[i]); }
    }

    eprintln!();
}

fn get_line_boundaries_at(index_start: usize, index_end: usize, code: &Code) -> (usize, usize) {
    let mut line_start = index_start;
    while line_start > 0 {
        if code[line_start - 1] == '\n' { break; }
        line_start -= 1;
    }

    let mut line_end = index_end;
    while line_end + 1 < code.len() {
        if let Some('\n') = code.get(line_end + 1) { break; }
        line_end += 1;
    }

    (line_start, line_end)
}
