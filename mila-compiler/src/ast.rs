use strum_macros::Display;

#[derive(Debug)]
pub struct ProgramInfo {
    pub name: String,
    pub functions: Vec<FunctionInfo>,
    pub entry_point: ScopeBlock,
    pub global_variables: Vec<Variable>,
    pub global_consts: Vec<Const>,
}

impl ProgramInfo {
    pub fn new(program_name: &str,
               functions: Vec<FunctionInfo>,
               entry_point: ScopeBlock,
               global_variables: Vec<Variable>,
               global_consts: Vec<Const>,
    ) -> Self {
        Self {
            name: program_name.to_owned(),
            entry_point,
            functions,
            global_variables,
            global_consts,
        }
    }
}

#[derive(Debug, Clone)]
pub struct ScopeBlock {
    pub local_consts: Vec<Const>,
    pub local_variables: Vec<Variable>,
    pub statements: Vec<Statement>,
}

impl ScopeBlock {
    pub fn new() -> Self {
        Self {
            local_consts: vec![],
            local_variables: vec![],
            statements: vec![],
        }
    }
}

#[derive(Debug, Clone)]
pub struct Const {
    pub identifier: String,
    pub value: i64,
}

#[derive(Debug, Clone)]
pub struct Variable {
    pub variable_type: VariableType,
    pub identifier: String,
}

impl Variable {
    pub fn new_integer(identifier: String) -> Self {
        Self {
            variable_type: VariableType::Integer,
            identifier,
        }
    }
}

#[derive(Display, Debug, Copy, Clone, PartialEq)]
pub enum VariableType {
    Integer,
    Array { from: i64, to: i64 },
}

#[derive(Debug, Clone)]
pub struct FunctionHeader {
    pub function_name: String,
    pub args: Vec<Variable>,
    pub is_function: bool,
}

#[derive(Debug)]
pub struct FunctionInfo {
    pub function_header: FunctionHeader,
    pub body: Option<ScopeBlock>, // for forward declarations
}

pub type StatementRef = Box<Statement>;

#[derive(Display, Debug, Clone)]
pub enum Statement {
    Block(ScopeBlock),
    Assignment(Assignable, Expr),
    JustExpression(Expr),
    Abort(Expr),
    Write(Vec<Printable>),
    WriteLn(Vec<Printable>),
    If { cond: Expr, true_branch: StatementRef, else_branch: Option<StatementRef> },
    While { cond: Expr, body: StatementRef },
    For(ForLoop),
    Inc(Assignable, i64),
    Exit,
    Break,
    Continue,
}

#[derive(Debug, Clone)]
pub struct ForLoop {
    pub var: Assignable,
    pub from: Expr,
    pub direction: ForLoopDirection,
    pub to: Expr,
    pub body: StatementRef,
}


#[derive(Display, Debug, Clone)]
pub enum ForLoopDirection {
    To,
    DownTo,
}


type ExprRef = Box<Expr>;


#[derive(Display, Debug, Clone, PartialEq)]
pub enum Expr {
    UnaryOp { op_type: UnaryOpType, arg: ExprRef },
    BinOp { lhs: ExprRef, op_type: BinaryOpType, rhs: ExprRef },
    FunctionCall { func_id: String, args: Vec<Expr> },
    ReadLn(Option<Assignable>),
    ArrayAccess { array_id: String, index: ExprRef },
    Var(String),
    Number(u64),
}

#[derive(Display, Debug, Copy, Clone, PartialEq)]
pub enum BinaryOpType {
    Arithmetic(ArithmeticBinaryOpType),
    Compare(BinaryCmpType),
    Logical(LogicalBinaryOpType),
}

#[derive(Display, Debug, Copy, Clone, PartialEq)]
pub enum ArithmeticBinaryOpType {
    Plus,
    Minus,
    Multiply,
    Divide,
    Modulo,
}

#[derive(Display, Debug, Copy, Clone, PartialEq)]
pub enum BinaryCmpType {
    IsEqual,
    IsNotEqual,
    Less,
    Greater,
    LessOrEqual,
    GreaterOrEqual,
}

#[derive(Display, Debug, Copy, Clone, PartialEq)]
pub enum LogicalBinaryOpType {
    LogicalOr,
    LogicalAnd,
}

#[derive(Display, Debug, Copy, Clone, PartialEq)]
pub enum UnaryOpType {
    UnaryMinus,
    UnaryPlus,
    Negation,
}

#[derive(Display, Debug, Clone, PartialEq)]
pub enum Assignable {
    ArrayAccess { array_id: String, index: ExprRef },
    Var(String),
}

pub struct ExpressionIsNotAssignable {}

impl TryInto<Assignable> for Expr {
    type Error = ExpressionIsNotAssignable;

    fn try_into(self) -> Result<Assignable, Self::Error> {
        Assignable::try_from_expr(self).ok_or(ExpressionIsNotAssignable {})
    }
}

impl Assignable {
    pub fn try_from_expr(expr: Expr) -> Option<Self> {
        match expr {
            Expr::ArrayAccess { array_id, index } => Some(
                Assignable::ArrayAccess { array_id, index }
            ),
            Expr::Var(identifier) => Some(
                Assignable::Var(identifier.clone())
            ),
            _ => None
        }
    }
}

#[derive(Display, Debug, Clone)]
pub enum Printable {
    Expression(Expr),
    StringLiteral(String),
}
