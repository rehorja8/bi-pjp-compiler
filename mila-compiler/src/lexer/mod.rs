use std::collections::HashMap;

use crate::lexer::automaton_transitions::get_all_transitions;
use crate::lexer::parse_error::{ParseError, PositionedParseError};
use crate::lexer::states::State;
use crate::tokens::{CodeCoordinate, CodeLocation, PositionedToken, Token};

mod automaton_transitions;
mod states;
pub(crate) mod parse_error;

pub struct Lexer<'a> {
    stream: &'a mut dyn Iterator<Item=char>,
    data: InternalData,
    transitions: HashMap<State, StateProperties>,
    current_char: Option<char>,
    has_returned_eof: bool,
    has_returned_error: bool,
    current_position: CodeCoordinate,
}

impl<'a> Lexer<'a> {
    pub fn new(stream: &'a mut dyn Iterator<Item=char>) -> Self {
        let transitions = HashMap::from_iter(get_all_transitions());

        let current_char = stream.next();
        Lexer {
            stream,
            data: InternalData::new(),
            transitions,
            current_char,
            has_returned_eof: false,
            has_returned_error: false,
            current_position: CodeCoordinate::new(),
        }
    }

    fn transition(&mut self, current_char: Option<char>) -> TransitionResult {
        let properties = &self.transitions[&self.data.current_state];
        let lazy_result = || (properties.get_token)(&self.data.accumulator);

        match current_char {
            Some(chr) => {
                for predicate in &properties.predicates {
                    if let Some(new_data) = predicate(chr, &self.data.accumulator) {
                        self.data = new_data;
                        return TransitionResult::Continue;
                    }
                }
            }
            None => {
                return TransitionResult::Finnish(lazy_result().map_err(|err| {
                    ParseError::new(format!("Unexpected eof: {}", err.message).as_str())
                }));
            }
        }

        return TransitionResult::Finnish(lazy_result());
    }

    pub fn next_token(&mut self) -> Result<PositionedToken, PositionedParseError> {
        if self.has_returned_eof {
            return Err(PositionedParseError::new_at_location(
                "next_token() has already returned eof",
                self.current_position,
                self.current_position)
            );
        }
        let mut starting_position = self.current_position;
        let mut end_position = self.current_position;

        loop {
            if self.current_char.is_none() && self.data.current_state == State::START {
                self.has_returned_eof = true;
                return Ok(PositionedToken::new(Token::Eof, self.current_position, self.current_position));
            }

            if let TransitionResult::Finnish(result) = self.transition(self.current_char) {
                self.data = InternalData::new();
                return result.map(
                    |token|
                        PositionedToken::new(token, starting_position, end_position)
                ).map_err(|err|
                    PositionedParseError::from(err, CodeLocation::new(starting_position, self.current_position)));
            }

            end_position = self.current_position;
            if let Some(chr) = self.current_char { self.current_position.update_with(chr); }
            if self.data.current_state == State::START { starting_position = self.current_position; }

            self.current_char = self.stream.next();
        }
    }
}

#[derive(Clone)]
struct InternalData {
    current_state: State,
    accumulator: Value,
}

impl InternalData {
    fn new() -> Self {
        InternalData {
            current_state: State::START,
            accumulator: Value::new(),
        }
    }

    fn just_state(state: State) -> Self {
        InternalData {
            current_state: state,
            accumulator: Value::new(),
        }
    }

    fn with(state: State, acc: Value) -> Self {
        InternalData {
            current_state: state,
            accumulator: acc,
        }
    }
}

type Predicate = dyn Fn(char, &Value) -> Option<InternalData>;
type TokenGetter = dyn Fn(&Value) -> Result<Token, ParseError>;

struct StateProperties {
    predicates: Vec<Box<Predicate>>,
    get_token: Box<TokenGetter>,
}

impl StateProperties {
    fn just_token_getter(get_token: Box<TokenGetter>) -> Self {
        StateProperties {
            predicates: Vec::new(),
            get_token,
        }
    }
}

#[derive(Clone)]
struct Value {
    integer: Option<u64>,
    string: String,
}

impl Value {
    fn string(s: String) -> Self {
        Self {
            integer: Some(0),
            string: s,
        }
    }

    fn option_integer(opt_n: Option<u64>) -> Self {
        Self {
            integer: opt_n,
            string: String::new(),
        }
    }

    fn integer(n: u64) -> Self {
        Self {
            integer: Some(n),
            string: String::new(),
        }
    }

    fn new() -> Self {
        Self {
            integer: Some(0),
            string: String::new(),
        }
    }
}

enum TransitionResult {
    Continue,
    Finnish(Result<Token, ParseError>),
}


impl<'a> Iterator for Lexer<'a> {
    type Item = Result<PositionedToken, PositionedParseError>;

    fn next(&mut self) -> Option<Self::Item> {
        if self.has_returned_eof || self.has_returned_eof {
            return None;
        }

        let item = self.next_token();
        if item.is_err() {
            self.has_returned_error = true;
        }

        return Some(item);
    }
}

