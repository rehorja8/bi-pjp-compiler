use std::collections::HashMap;
use crate::lexer::{InternalData, Predicate, State, StateProperties, Value};
use crate::lexer::parse_error::ParseError;
use crate::tokens::{KEYWORD_MAP, KeywordType, Token};

pub(super) fn always_fail(error_message: &str) -> Box<dyn Fn(&Value) -> Result<Token, ParseError> + '_> {
    Box::new(|_| Err(ParseError::new(error_message)))
}

pub(super) fn always_return_token(token: Token) -> Box<dyn Fn(&Value) -> Result<Token, ParseError> + 'static> {
    Box::new(move |_| Ok(token.clone()))
}

pub(super) fn map(
    pairs: impl Iterator<Item=(char, State)>,
) -> Box<Predicate> {
    let map = HashMap::<char, State>::from_iter(pairs);
    Box::new(move |chr, acc| {
        map.get(&chr)
            .map(|state| InternalData::with(*state, acc.clone()))
    })
}

pub(super) fn sink_state(state: State, token: Token) -> (State, StateProperties) {
    (
        state,
        StateProperties::just_token_getter(Box::new(move |_| Ok(token.clone()))),
    )
}

pub(super) fn sink_state_keyword(state: State, keyword_type: KeywordType) -> (State, StateProperties) {
    sink_state(state, Token::Keyword(keyword_type))
}

pub(super) fn two_option_state(state: State, first_option_token: Token, second_option_char: char, second_option_state: State) -> (State, StateProperties) {
    (
        state,
        StateProperties {
            predicates: vec![Box::new(
                move |chr, _|
                    if chr == second_option_char {
                        Some(InternalData::just_state(second_option_state))
                    } else {
                        None
                    })],
            get_token: always_return_token(first_option_token),
        }
    )
}

pub(super) fn get_all_transitions() -> [(State, StateProperties); 31] {
    [
        (
            State::START,
            StateProperties {
                predicates: vec![
                    Box::new(|chr, _| {
                        if chr.is_whitespace() {
                            Some(InternalData::just_state(State::START))
                        } else {
                            None
                        }
                    }),
                    Box::new(|chr, _| {
                        if chr.is_alphabetic() || chr == '_' {
                            Some(InternalData::with(
                                State::IDENTIFIER___KEYWORD,
                                Value::string(chr.to_string()),
                            ))
                        } else {
                            None
                        }
                    }),
                    map([
                        ('<', State::LESS___LESS_EQUAL___NOT_EQUAL),
                        ('>', State::MORE___MORE_EQUAL),
                        ('(', State::LEFT_PARENTHESIS),
                        (')', State::RIGHT_PARENTHESIS),
                        ('[', State::LEFT_SQUARE_BRACKET),
                        (']', State::RIGHT_SQUARE_BRACKET),
                        ('+', State::PLUS),
                        ('-', State::MINUS),
                        ('!', State::LOGICAL_NOT),
                        ('=', State::EQUAL_SIGN),
                        (':', State::COLON___ASSIGN),
                        (';', State::SEMICOLON),
                        ('*', State::MULTIPLY),
                        ('.', State::DOT___DOUBLE_DOT),
                        (',', State::COMMA),
                        ('{', State::COMMENT),
                        ('$', State::NUMBER_16_BEGINNING),
                        ('&', State::NUMBER_08_BEGINNING),
                        ('\'', State::STRING_LITERAL),
                    ]
                        .into_iter()),
                    Box::new(|chr, _| {
                        chr.to_digit(10).map(|value| {
                            InternalData::with(State::NUMBER_10, Value::integer(value as u64))
                        })
                    }),
                ],
                get_token: always_fail("Invalid character"),
            },
        ),
        (
            State::IDENTIFIER___KEYWORD,
            StateProperties {
                predicates: vec![Box::new(|chr, prev_acc| {
                    if chr.is_alphanumeric() || chr == '_' {
                        Some(InternalData::with(
                            State::IDENTIFIER___KEYWORD,
                            Value::string({
                                let mut new_acc = prev_acc.string.clone();
                                new_acc.push(chr);
                                new_acc
                            }),
                        ))
                    } else {
                        None
                    }
                })],
                get_token: Box::new(|acc| match KEYWORD_MAP.get(&acc.string) {
                    Some(keyword_type) => Ok(Token::Keyword(*keyword_type)),
                    None => Ok(Token::Identifier(acc.string.clone())),
                }),
            },
        ),
        (
            State::NUMBER_10,
            StateProperties {
                predicates: vec![Box::new(
                    |chr, acc|
                        acc.integer.and_then(|accumulated_value|
                            chr.to_digit(10)
                                .map(|current_value|
                                    InternalData::with(
                                        State::NUMBER_10,
                                        Value::option_integer(
                                            accumulated_value.checked_mul(10)
                                                .and_then(|value|
                                                    (current_value as u64).checked_add(value))),
                                    )))
                )],
                get_token: Box::new(|value|
                    value.integer.map(Token::IntLiteral).ok_or(ParseError::new("Integer literal is too big"))),
            },
        ),
        (
            State::COMMENT,
            StateProperties {
                predicates: vec![Box::new(|chr, _| {
                    if chr == '}' {
                        Some(InternalData::just_state(State::START))
                    } else {
                        Some(InternalData::just_state(State::COMMENT))
                    }
                })],
                get_token: always_fail("Non closing comment"),
            },
        ),
        (
            State::STRING_LITERAL,
            StateProperties {
                predicates: vec![Box::new(|chr, acc| {
                    if chr == '\'' {
                        Some(InternalData {
                            current_state: State::STRING_LITERAL_END,
                            accumulator: acc.clone(),
                        })
                    } else {
                        Some(InternalData::with(
                            State::STRING_LITERAL,
                            Value::string({
                                let mut acc = acc.string.clone();
                                acc.push(chr);
                                acc
                            }),
                        ))
                    }
                })],
                get_token: always_fail("Unmatched end of a string literal, expected: '"),
            },
        ),
        (
            State::STRING_LITERAL_END,
            StateProperties::just_token_getter(Box::new(|acc| {
                Ok(Token::StringLiteral(acc.string.clone()))
            })),
        ),
        (
            State::NUMBER_08_BEGINNING,
            StateProperties {
                predicates: vec![Box::new(
                    |chr, _| chr.to_digit(8).map(
                        |current_value| InternalData::with(
                            State::NUMBER_08,
                            Value::integer(current_value as u64),
                        )))],

                get_token: always_fail("'&' must be followed by an int literal in a octal base"),
            }
        ),
        (
            State::NUMBER_08,
            StateProperties {
                predicates: vec![Box::new(
                    |chr, acc|
                        acc.integer.and_then(|accumulated_value|
                            chr.to_digit(8)
                                .map(|current_value|
                                    InternalData::with(
                                        State::NUMBER_08,
                                        Value::option_integer(
                                            accumulated_value.checked_mul(8)
                                                .and_then(|value|
                                                    (current_value as u64).checked_add(value))),
                                    )))
                )],
                get_token: Box::new(|value|
                    value.integer.map(Token::IntLiteral).ok_or(ParseError::new("Integer literal is too big"))),
            },
        ),
        (
            State::NUMBER_16_BEGINNING,
            StateProperties {
                predicates: vec![Box::new(|chr, _| chr.to_digit(16).map(|current_value| InternalData::with(
                    State::NUMBER_16,
                    Value::integer(current_value as u64),
                )))],

                get_token: always_fail("'$' must be followed by an int literal in a hexadecimal base"),
            }
        ),
        (
            State::NUMBER_16,
            StateProperties {
                predicates: vec![Box::new(
                    |chr, acc|
                        acc.integer.and_then(|accumulated_value|
                            chr.to_digit(16)
                                .map(|current_value|
                                    InternalData::with(
                                        State::NUMBER_16,
                                        Value::option_integer(
                                            accumulated_value.checked_mul(16)
                                                .and_then(|value|
                                                    (current_value as u64).checked_add(value))),
                                    )))
                )],
                get_token: Box::new(|value|
                    value.integer.map(Token::IntLiteral).ok_or(ParseError::new("Integer literal is too big"))),
            },
        ),
        (
            State::LESS___LESS_EQUAL___NOT_EQUAL,
            StateProperties {
                predicates: vec![Box::new(|chr, _| match chr {
                    '=' => Some(InternalData::just_state(State::LESS_EQUAL)),
                    '>' => Some(InternalData::just_state(State::NOT_EQUAL)),
                    _ => None,
                })],
                get_token: always_return_token(Token::Keyword(KeywordType::Less)),
            }
        ),
        two_option_state(State::COLON___ASSIGN, Token::Keyword(KeywordType::Colon), '=', State::ASSIGN),
        two_option_state(State::MORE___MORE_EQUAL, Token::Keyword(KeywordType::More), '=', State::MORE_EQUAL),
        two_option_state(State::DOT___DOUBLE_DOT, Token::Keyword(KeywordType::Dot), '.', State::DOUBLE_DOT),
        sink_state_keyword(State::LOGICAL_NOT, KeywordType::LogicalNot),
        sink_state_keyword(State::COMMA, KeywordType::Comma),
        sink_state_keyword(State::DOUBLE_DOT, KeywordType::DoubleDot),
        sink_state_keyword(State::PLUS, KeywordType::Plus),
        sink_state_keyword(State::MULTIPLY, KeywordType::Mult),
        sink_state_keyword(State::MINUS, KeywordType::Mult),
        sink_state_keyword(State::RIGHT_PARENTHESIS, KeywordType::ParentClose),
        sink_state_keyword(State::LEFT_PARENTHESIS, KeywordType::ParentOpen),
        sink_state_keyword(State::RIGHT_SQUARE_BRACKET, KeywordType::RightSquareBracket),
        sink_state_keyword(State::LEFT_SQUARE_BRACKET, KeywordType::LeftSquareBracket),
        sink_state_keyword(State::MINUS, KeywordType::Minus),
        sink_state_keyword(State::SEMICOLON, KeywordType::Semicolon),
        sink_state_keyword(State::MORE_EQUAL, KeywordType::MoreOrEqual),
        sink_state_keyword(State::LESS_EQUAL, KeywordType::LessOrEqual),
        sink_state_keyword(State::NOT_EQUAL, KeywordType::NotEq),
        sink_state_keyword(State::EQUAL_SIGN, KeywordType::EqualSign),
        sink_state_keyword(State::ASSIGN, KeywordType::Assign),
    ]
}
