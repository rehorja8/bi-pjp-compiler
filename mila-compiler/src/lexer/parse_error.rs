use std::error::Error;
use std::fmt;
use std::fmt::Formatter;
use crate::tokens::{CodeCoordinate, CodeLocation, Token};

#[derive(Debug, PartialEq, Clone)]
pub struct ParseError {
    pub message: String,
}

impl ParseError {
    pub fn new(message: &str) -> ParseError {
        ParseError {
            message: message.to_owned(),
        }
    }
}

impl Error for ParseError {}

impl fmt::Display for ParseError {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(f, "Parse error: \"{}\"", self.message)
    }
}

#[derive(Debug, Clone)]
pub struct PositionedParseError {
    pub error: ParseError,
    pub position: CodeLocation,
}

impl fmt::Display for PositionedParseError {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "{} at {}-{}", self.error, self.position.start, self.position.end)
    }
}

impl Error for PositionedParseError {}

impl PositionedParseError {
    pub fn new(message: &str, position: CodeLocation) -> Self {
        Self {
            error: ParseError::new(message),
            position,
        }
    }

    pub fn new_at_location(message: &str, start_position: CodeCoordinate, end_position: CodeCoordinate) -> Self {
        Self {
            error: ParseError::new(message),
            position: CodeLocation::new(start_position, end_position),
        }
    }

    pub fn expected_assignable(position: CodeLocation) -> Self {
        Self {
            error: ParseError::new("expected assignable (meaning integer identifier or array access)"),
            position: CodeLocation::new(position.start, position.end),
        }
    }

    pub fn from(error: ParseError, position: CodeLocation) -> Self {
        Self {
            error,
            position,
        }
    }

    pub fn else_block_when_if_under_if(position: CodeLocation) -> Self {
        Self {
            position,
            error: ParseError::new(
                "To avoid confusion, you cannot have `else branch` when you have `if` under `if` with no `begin-end` block.\n\
                         To surpass this, you have remove the `else` or add `begin-end` block around the second `if` :)"
            ),
        }
    }

    pub fn unexpected_token(position: CodeLocation, got: &Token, expected: &[&str]) -> Self {
        Self {
            position,
            error: ParseError::new(
                format!("unexpected token: \"{got}\", expected maybe one of: {:?}", expected).as_str()
            ),
        }
    }
}
