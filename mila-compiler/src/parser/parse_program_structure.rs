use crate::ast::{Assignable, Const, ForLoop, ForLoopDirection, FunctionHeader, FunctionInfo, Printable, ProgramInfo, ScopeBlock, Statement, Variable, VariableType};
use crate::lexer::parse_error::PositionedParseError;
use crate::tokens::{CodeLocation, KeywordType, PositionedToken, Token};
use super::Parser;


impl<LexerIterator> Parser<LexerIterator>
    where LexerIterator: Iterator<Item=Result<PositionedToken, PositionedParseError>> {
    pub(super) fn parse_program(&mut self) -> Result<ProgramInfo, PositionedParseError> {
        // Parse program header - `program name;`
        self.match_keyword(KeywordType::Program)?;
        let PositionedToken { token, position } = self.lexer.next().unwrap()?;
        let program_name;
        if let Token::Identifier(identifier) = token {
            program_name = identifier;
        } else {
            return Err(PositionedParseError::unexpected_token(position, &token, &["name of the program"]));
        }
        self.match_keyword(KeywordType::Semicolon)?;

        // Parse global variables and consts
        let mut global_variables = Vec::new();
        let mut global_consts = Vec::new();
        self.parse_var_and_const_declaration(&mut global_variables, &mut global_consts)?;

        // Parse function declarations
        let declared_functions = self.parse_function_declarations()?;

        // Parse the rest of the program ("main" function)
        let entry_point = self.match_scope_block()?;

        // Match final dot
        self.match_keyword(KeywordType::Dot)?;
        self.match_token(&Token::Eof)?;

        Ok(ProgramInfo::new(program_name.as_str(), declared_functions, entry_point, global_variables, global_consts))
    }

    fn parse_function_declarations(&mut self) -> Result<Vec<FunctionInfo>, PositionedParseError> {
        let mut functions = Vec::<FunctionInfo>::new();

        loop {
            match self.lexer_peek()?.token {
                Token::Keyword(KeywordType::Function) |
                Token::Keyword(KeywordType::Procedure) => {}
                _ => break,
            }
            let function_header = self.match_function_header()?;

            let PositionedToken { token, position } = self.lexer_peek()?;
            let body = match token {
                Token::Keyword(KeywordType::Forward) => {
                    self.lexer.next();
                    None
                }

                Token::Keyword(KeywordType::Var | KeywordType::Const | KeywordType::Begin)
                => { Some(self.match_scope_block()?) }

                _ => return Err(
                    PositionedParseError::unexpected_token(
                        *position, token, &["forward;", "begin-end block as a body of the function/procedure"],
                    ))
            };
            self.match_keyword(KeywordType::Semicolon)?;

            functions.push(FunctionInfo { function_header, body });
        }

        Ok(functions)
    }

    fn match_function_header(&mut self) -> Result<FunctionHeader, PositionedParseError> {
        let PositionedToken { token, position } = self.lexer_peek()?;
        let is_function;
        match token {
            Token::Keyword(KeywordType::Function) => { is_function = true }
            Token::Keyword(KeywordType::Procedure) => { is_function = false }
            _ => return Err(PositionedParseError::unexpected_token(*position, token, &["function", "procedure"]))
        }
        self.lexer.next();
        let function_name = self.match_identifier()?;
        self.match_keyword(KeywordType::ParentOpen)?;
        let args = self.parse_function_args_declaration()?;
        self.match_keyword(KeywordType::ParentClose)?;

        if is_function {
            self.match_keyword(KeywordType::Colon)?;
            let PositionedToken { token, position } = self.lexer_peek()?;
            match token {
                Token::Keyword(KeywordType::Integer) => { self.lexer.next(); }
                Token::Keyword(KeywordType::Array) => {
                    return Err(PositionedParseError::new("Sorry, functions returning an array is not supported", *position));
                }
                _ => return Err(PositionedParseError::unexpected_token(*position, token, &["integer", "(not supported) array [...]"]))
            }
        }
        self.match_keyword(KeywordType::Semicolon)?;

        Ok(FunctionHeader { function_name, args, is_function })
    }

    fn parse_function_args_declaration(&mut self) -> Result<Vec<Variable>, PositionedParseError> {
        let mut args = Vec::<Variable>::new();
        loop {
            let token = &self.lexer_peek()?.token;
            if *token == Token::Keyword(KeywordType::ParentClose) {
                break;
            }
            if !args.is_empty() {
                self.match_keyword(KeywordType::Semicolon)?;
            }

            let identifier = self.match_identifier()?;
            self.match_keyword(KeywordType::Colon)?;
            let arg_type = self.match_variable_type()?;

            args.push(Variable { variable_type: arg_type, identifier });
        }

        Ok(args)
    }

    fn match_scope_block(&mut self) -> Result<ScopeBlock, PositionedParseError> {
        let mut local_variables = Vec::new();
        let mut local_consts = Vec::new();
        self.parse_var_and_const_declaration(&mut local_variables, &mut local_consts)?;

        self.match_keyword(KeywordType::Begin)?;
        let statements = self.parse_list_of_statements()?;
        self.match_keyword(KeywordType::End)?;

        Ok(ScopeBlock { local_consts, local_variables, statements })
    }

    fn parse_var_and_const_declaration(&mut self, vars: &mut Vec<Variable>, consts: &mut Vec<Const>) -> Result<(), PositionedParseError> {
        loop {
            let PositionedToken { token, position } = self.lexer_peek()?;
            match token {
                Token::Keyword(KeywordType::Var) => {
                    vars.extend(self.match_var_declaration()?.into_iter());
                }
                Token::Keyword(KeywordType::Const) => {
                    consts.extend(self.match_const_declaration()?.into_iter());
                }
                Token::Keyword(KeywordType::Begin | KeywordType::Function | KeywordType::Procedure) => return Ok(()),
                _ => return Err(PositionedParseError::unexpected_token(*position, token, &["var", "const", "begin", "function (only for global vars)", "procedure (only for global vars)"])),
            }
        }
    }

    fn match_const_declaration(&mut self) -> Result<Vec<Const>, PositionedParseError> {
        let mut consts = Vec::new();

        self.match_keyword(KeywordType::Const)?;
        while let PositionedToken { token: Token::Identifier(_), position: _ } = self.lexer_peek()? {
            let identifier = self.match_identifier()?;
            self.match_keyword(KeywordType::EqualSign)?;
            let value = self.match_signed_int_literal()?;
            self.match_keyword(KeywordType::Semicolon)?;

            consts.push(Const { identifier, value });
        }

        Ok(consts)
    }

    fn match_var_declaration(&mut self) -> Result<Vec<Variable>, PositionedParseError> {
        self.match_keyword(KeywordType::Var)?;
        let PositionedToken { token, position } = self.lexer_peek()?;
        match token {
            Token::Identifier(_) => {}
            _ => return Err(PositionedParseError::unexpected_token(*position, token, &["identifier"])),
        }

        let mut variables = Vec::<Variable>::new();
        while let PositionedToken { token: Token::Identifier(_), position: _ } = self.lexer_peek()? {
            let mut identifiers = Vec::<String>::new();
            identifiers.push(self.match_identifier()?);

            // parse individual identifiers
            loop {
                let PositionedToken { token, position } = self.lexer.next().unwrap()?;
                match token {
                    Token::Keyword(KeywordType::Colon) => break,
                    Token::Keyword(KeywordType::Comma) => {
                        identifiers.push(self.match_identifier()?);
                    }
                    _ => return Err(PositionedParseError::unexpected_token(
                        position,
                        &token,
                        &[": integer", ": array [...] of integer", ", identifier"])
                    )
                }
            }

            let variable_type = self.match_variable_type()?;
            self.match_keyword(KeywordType::Semicolon)?;
            variables.extend(identifiers
                .into_iter()
                .map(|identifier| Variable { variable_type, identifier })
            )
        }

        Ok(variables)
    }

    fn match_variable_type(&mut self) -> Result<VariableType, PositionedParseError> {
        let PositionedToken { token, position } = self.lexer_peek()?;
        match token {
            Token::Keyword(KeywordType::Integer) => {
                self.lexer.next();
                Ok(VariableType::Integer)
            }
            Token::Keyword(KeywordType::Array) => {
                self.lexer.next();
                self.match_keyword(KeywordType::LeftSquareBracket)?;
                let lower_bound = self.match_signed_int_literal()?;
                self.match_keyword(KeywordType::DoubleDot)?;
                let upper_bound = self.match_signed_int_literal()?;
                self.match_keyword(KeywordType::RightSquareBracket)?;
                self.match_keyword(KeywordType::Of)?;
                self.match_keyword(KeywordType::Integer)?;

                Ok(VariableType::Array { from: lower_bound, to: upper_bound })
            }
            _ => Err(PositionedParseError::unexpected_token(*position, token, &["integer", "array [..] of integer"]))
        }
    }

    fn parse_list_of_statements(&mut self) -> Result<Vec<Statement>, PositionedParseError> {
        let mut statements = Vec::<Statement>::new();

        loop {
            // If we are not at the first statement, we want to assume at least one
            if !statements.is_empty() { self.match_keyword(KeywordType::Semicolon)?; }

            // Consume rest of the semicolons
            while Token::Keyword(KeywordType::Semicolon) == self.lexer_peek()?.token { self.lexer.next(); }

            if Token::Keyword(KeywordType::End) == self.lexer_peek()?.token {
                break;
            }
            statements.push(self.parse_statement()?);
            // The control flow is a bit funky here, but it works
            if Token::Keyword(KeywordType::End) == self.lexer_peek()?.token {
                break;
            }
        }

        Ok(statements)
    }

    fn parse_statement(&mut self) -> Result<Statement, PositionedParseError> {
        self.parse_statement_if_sensitive(false)
    }

    fn parse_statement_if_sensitive(&mut self, is_under_if: bool) -> Result<Statement, PositionedParseError> {
        let PositionedToken { token, position } = self.lexer_peek()?.clone();
        match token {
            Token::Keyword(KeywordType::Abort) => {
                self.lexer.next();
                let exit_code_expr = self.parse_expr()?;
                Ok(Statement::Abort(exit_code_expr))
            }
            Token::Keyword(KeywordType::Exit) => {
                self.lexer.next();
                Ok(Statement::Exit)
            }
            Token::Keyword(KeywordType::Break) => {
                self.lexer.next();
                Ok(Statement::Break)
            }
            Token::Keyword(KeywordType::Continue) => {
                self.lexer.next();
                Ok(Statement::Continue)
            }
            Token::Keyword(KeywordType::Write) |
            Token::Keyword(KeywordType::WriteLn) => {
                self.lexer.next();
                self.match_keyword(KeywordType::ParentOpen)?;
                let printables = self.match_list_of_printables()?;
                self.match_keyword(KeywordType::ParentClose)?;
                match token {
                    Token::Keyword(KeywordType::Write) => Ok(Statement::Write(printables)),
                    Token::Keyword(KeywordType::WriteLn) => Ok(Statement::WriteLn(printables)),
                    _ => unreachable!("other cases should be covered in the match above")
                }
            }
            Token::Keyword(KeywordType::Dec) |
            Token::Keyword(KeywordType::Inc) => {
                self.lexer.next();
                self.match_keyword(KeywordType::ParentOpen)?;
                let expression = self.parse_expr()?;
                let assignable_value = Assignable::try_from_expr(expression)
                    .ok_or(PositionedParseError::new("Expected assignable expression - variable or array access.", position))?;
                self.match_keyword(KeywordType::ParentClose)?;
                match token {
                    Token::Keyword(KeywordType::Dec) => Ok(Statement::Inc(assignable_value, -1)),
                    Token::Keyword(KeywordType::Inc) => Ok(Statement::Inc(assignable_value, 1)),
                    _ => unreachable!("other cases should be covered in the match above")
                }
            }

            Token::IntLiteral(_) | Token::Identifier(_)
            | Token::Keyword(KeywordType::Minus | KeywordType::Plus | KeywordType::LogicalNot | KeywordType::ReadLn) => {
                let expression = self.parse_expr()?;
                if Token::Keyword(KeywordType::Assign) == self.lexer_peek()?.token {
                    let lhs_expression = Assignable::try_from_expr(expression)
                        .ok_or(PositionedParseError::new("When assigning, left side of the ':=' must be assignable - variable of array access.", position))?;
                    self.lexer.next();
                    let rhs_expression = self.parse_expr()?;
                    return Ok(Statement::Assignment(lhs_expression, rhs_expression));
                }

                Ok(Statement::JustExpression(expression))
            }

            Token::Keyword(KeywordType::Begin | KeywordType::Var | KeywordType::Const) => {
                let block = self.match_scope_block()?;
                Ok(Statement::Block(block))
            }

            Token::Keyword(KeywordType::If) => {
                self.match_if_statement(is_under_if)
            }
            Token::Keyword(KeywordType::While) => {
                self.match_while_statement()
            }
            Token::Keyword(KeywordType::For) => {
                self.match_for_statement(position)
            }
            _ => Err(PositionedParseError::unexpected_token(position, &token, &["any statement"]))
        }
    }

    fn match_list_of_printables(&mut self) -> Result<Vec<Printable>, PositionedParseError> {
        let mut printables = Vec::new();
        loop {
            if self.lexer_peek()?.token == Token::Keyword(KeywordType::ParentClose) {
                return Ok(printables);
            }
            if !printables.is_empty() { self.match_keyword(KeywordType::Comma)?; }
            printables.push(self.match_printable()?);
        }
    }

    fn match_printable(&mut self) -> Result<Printable, PositionedParseError> {
        if let Token::StringLiteral(str) = &self.lexer_peek()?.token {
            let str = str.clone();
            self.lexer.next();
            return Ok(Printable::StringLiteral(str));
        }
        let expression = self.parse_expr().map_err(|e| PositionedParseError::new(
            format!("Expected a printable expression (numeric or a string literal). This does not look like a string literal, so started parsing numeric expression.\nError while parsing numeric expression: {}", e.error.message).as_str(),
            e.position,
        ))?;

        Ok(Printable::Expression(expression))
    }

    fn match_if_statement(&mut self, is_under_another_if: bool) -> Result<Statement, PositionedParseError> {
        self.match_keyword(KeywordType::If)?;
        let cond = self.parse_expr()?;
        self.match_keyword(KeywordType::Then)?;
        let true_branch = self.parse_statement_if_sensitive(true)?;

        let PositionedToken { token: next_token, position } = self.lexer_peek()?;

        if let Statement::If { .. } = true_branch {
            if *next_token == Token::Keyword(KeywordType::Else) {
                return Err(PositionedParseError::else_block_when_if_under_if(*position));
            }
        }

        let else_branch =
            if Token::Keyword(KeywordType::Else) == *next_token && !is_under_another_if {
                self.lexer.next();
                Some(self.parse_statement()?)
            } else {
                None
            };

        let true_branch = Box::new(true_branch);
        let else_branch = else_branch.map(Box::new);

        Ok(Statement::If {
            cond,
            true_branch,
            else_branch,
        })
    }
    fn match_while_statement(&mut self) -> Result<Statement, PositionedParseError> {
        self.match_keyword(KeywordType::While)?;
        let condition = self.parse_expr()?;
        self.match_keyword(KeywordType::Do)?;

        let body = self.parse_statement()?;

        Ok(Statement::While { cond: condition, body: Box::new(body) })
    }
    fn match_for_statement(&mut self, position: CodeLocation) -> Result<Statement, PositionedParseError> {
        self.match_keyword(KeywordType::For)?;
        let iterator = self.parse_expr()
            .and_then(
                |expression| Assignable::try_from_expr(expression)
                    .ok_or_else(|| PositionedParseError::expected_assignable(position))
            )?;
        self.match_keyword(KeywordType::Assign)?;
        let start = self.parse_expr()?;

        let PositionedToken { token, position } = self.lexer_peek()?;
        let direction = match token {
            Token::Keyword(KeywordType::To) => ForLoopDirection::To,
            Token::Keyword(KeywordType::DownTo) => ForLoopDirection::DownTo,
            _ => return Err(PositionedParseError::unexpected_token(*position, token, &["to", "downto"])),
        };
        self.lexer.next();

        let end = self.parse_expr()?;
        self.match_keyword(KeywordType::Do)?;
        let body = Box::new(self.parse_statement()?);

        Ok(Statement::For(
            ForLoop {
                var: iterator,
                from: start,
                direction,
                to: end,
                body,
            }
        ))
    }
}



