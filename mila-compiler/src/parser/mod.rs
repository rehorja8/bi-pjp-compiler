use std::iter::Peekable;

use crate::ast::{Expr, ProgramInfo};
use crate::lexer::parse_error::PositionedParseError;
use crate::tokens::{KeywordType, PositionedToken, Token};

mod parse_expression;
mod parse_program_structure;

pub struct Parser<LexerIterator>
    where LexerIterator: Iterator<Item=Result<PositionedToken, PositionedParseError>>
{
    lexer: Peekable<LexerIterator>,
}

impl<LexerIterator> Parser<LexerIterator>
    where LexerIterator: Iterator<Item=Result<PositionedToken, PositionedParseError>> {
    pub fn from_lexer(lexer: LexerIterator) -> Self {
        Self { lexer: lexer.peekable() }
    }

    pub fn generate_ast(&mut self) -> Result<ProgramInfo, PositionedParseError> {
        self.parse_program()
    }

    fn parse_expr(&mut self) -> Result<Expr, PositionedParseError> {
        self.parse_logical_or()
    }

    fn lexer_peek(&mut self) -> Result<&PositionedToken, PositionedParseError> {
        self.lexer.peek().unwrap().as_ref().map_err(|e| e.clone())
    }

    fn match_token(&mut self, expected: &Token) -> Result<(), PositionedParseError> {
        let PositionedToken { position, token } = self.lexer.next().unwrap()?;

        if *expected != token {
            return Err(PositionedParseError::unexpected_token(
                position, &token,
                &[format!("{}", *expected).as_str()]));
        }

        Ok(())
    }

    fn match_keyword(&mut self, expected: KeywordType) -> Result<(), PositionedParseError> {
        self.match_token(&Token::Keyword(expected))
    }
}


