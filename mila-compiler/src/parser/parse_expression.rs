use crate::ast::{ArithmeticBinaryOpType, BinaryCmpType, BinaryOpType, Expr, LogicalBinaryOpType, UnaryOpType};
use crate::lexer::parse_error::PositionedParseError;
use crate::tokens::{PositionedToken, Token};
use crate::tokens::KeywordType;
use super::Parser;

impl<LexerIterator> Parser<LexerIterator>
    where LexerIterator: Iterator<Item=Result<PositionedToken, PositionedParseError>> {
    /// expr0:
    /// - logical operator: `||`
    pub(super) fn parse_logical_or(&mut self) -> Result<Expr, PositionedParseError> {
        let lhs_expression = self.parse_logical_and()?;
        let PositionedToken { token, position } = self.lexer_peek()?.clone();

        use KeywordType::*;
        match token {
            Token::Keyword(Or) => {
                self.lexer.next();
                let rhs_expression = self.parse_logical_and()?;
                Ok(Expr::BinOp {
                    lhs: Box::new(lhs_expression),
                    rhs: Box::new(rhs_expression),
                    op_type: BinaryOpType::Logical(LogicalBinaryOpType::LogicalOr),
                })
            }
            Token::Keyword(
                Then | Assign | Semicolon | End | ParentClose | Do | To | DownTo
                | RightSquareBracket | Comma | Else
            ) => Ok(lhs_expression),
            _ => Err(PositionedParseError::unexpected_token(position, &token, &["operator ||", "..."]))
        }
    }

    /// expr1:
    /// - logical operator: `&&`
    fn parse_logical_and(&mut self) -> Result<Expr, PositionedParseError> {
        let lhs_expression = self.parse_equality_op()?;
        let PositionedToken { token, position } = self.lexer_peek()?.clone();

        use KeywordType::*;
        match token {
            Token::Keyword(And) => {
                self.lexer.next();
                let rhs_expression = self.parse_equality_op()?;
                Ok(Expr::BinOp {
                    lhs: Box::new(lhs_expression),
                    rhs: Box::new(rhs_expression),
                    op_type: BinaryOpType::Logical(LogicalBinaryOpType::LogicalAnd),
                })
            }
            Token::Keyword(
                Or | Then | Assign | Semicolon | End | ParentClose | Do | To | DownTo
                | RightSquareBracket | Comma | Else
            ) => Ok(lhs_expression),
            _ => Err(PositionedParseError::unexpected_token(position, &token, &["operator &&", "..."]))
        }
    }

    /// expr2:
    /// - equality operators:
    ///     - `<>`
    ///     - `=`
    fn parse_equality_op(&mut self) -> Result<Expr, PositionedParseError> {
        let lhs_expression = self.parse_relational_op()?;
        let PositionedToken { token, position } = self.lexer_peek()?.clone();

        use KeywordType::*;
        match token {
            Token::Keyword(EqualSign | NotEq) => {
                self.lexer.next();
                let rhs_expression = self.parse_relational_op()?;
                Ok(Expr::BinOp {
                    lhs: Box::new(lhs_expression),
                    rhs: Box::new(rhs_expression),
                    op_type: match token {
                        Token::Keyword(EqualSign) => BinaryOpType::Compare(BinaryCmpType::IsEqual),
                        Token::Keyword(NotEq) => BinaryOpType::Compare(BinaryCmpType::IsNotEqual),
                        _ => unreachable!("this should not be reached because this case was checked before"),
                    },
                })
            }
            Token::Keyword(
                And | Or | Then | Assign | Semicolon | End | ParentClose | Do | To | DownTo
                | RightSquareBracket | Comma | Else
            ) => Ok(lhs_expression),
            _ => Err(PositionedParseError::unexpected_token(position, &token, &["relational operator <, <=, >, >=", "..."]))
        }
    }

    /// expr3:
    /// - relational operators:
    ///     - `<`
    ///     - `>`
    ///     - `<=`
    ///     - `>=`
    fn parse_relational_op(&mut self) -> Result<Expr, PositionedParseError> {
        let lhs_expression = self.parse_binary_op1()?;
        let PositionedToken { token, position } = self.lexer_peek()?.clone();

        use KeywordType::*;
        match token {
            Token::Keyword(Less | LessOrEqual | More | MoreOrEqual) => {
                self.lexer.next();
                let rhs_expression = self.parse_binary_op1()?;
                Ok(Expr::BinOp {
                    lhs: Box::new(lhs_expression),
                    rhs: Box::new(rhs_expression),
                    op_type: match token {
                        Token::Keyword(Less) => BinaryOpType::Compare(BinaryCmpType::Less),
                        Token::Keyword(LessOrEqual) => BinaryOpType::Compare(BinaryCmpType::LessOrEqual),
                        Token::Keyword(More) => BinaryOpType::Compare(BinaryCmpType::Greater),
                        Token::Keyword(MoreOrEqual) => BinaryOpType::Compare(BinaryCmpType::GreaterOrEqual),
                        _ => unreachable!("this should not be reached because this case was checked before"),
                    },
                })
            }
            Token::Keyword(
                EqualSign | NotEq | And
                | Or | Then | Assign | Semicolon | End | ParentClose | Do | To | DownTo
                | RightSquareBracket | Comma | Else
            ) => Ok(lhs_expression),
            _ => Err(PositionedParseError::unexpected_token(position, &token, &["relational operator <, <=, >, >=", "..."]))
        }
    }

    /// expr4:
    /// - binary operators:
    ///     - `+`
    ///     - `-`
    fn parse_binary_op1(&mut self) -> Result<Expr, PositionedParseError> {
        let mut lhs_expression = self.parse_binary_op2()?;

        loop {
            use KeywordType::*;

            let PositionedToken { token, position } = self.lexer_peek()?.clone();
            match token {
                Token::Keyword(Plus | Minus) => {
                    self.lexer.next();
                    let rhs_expression = self.parse_binary_op2()?;
                    lhs_expression = Expr::BinOp {
                        lhs: Box::new(lhs_expression),
                        rhs: Box::new(rhs_expression),
                        op_type: match token {
                            Token::Keyword(Plus) => BinaryOpType::Arithmetic(ArithmeticBinaryOpType::Plus),
                            Token::Keyword(Minus) => BinaryOpType::Arithmetic(ArithmeticBinaryOpType::Minus),
                            _ => unreachable!("this should not be reached because this case was checked before"),
                        },
                    }
                }
                Token::Keyword(
                    Less | LessOrEqual | More | MoreOrEqual | EqualSign | NotEq | And
                    | Or | Then | Assign | Semicolon | End | ParentClose | Do | To | DownTo
                    | RightSquareBracket | Comma | Else
                ) => break,
                _ => return Err(PositionedParseError::unexpected_token(position, &token, &["+", "-", "..."]))
            }
        }

        Ok(lhs_expression)
    }

    /// expr5:
    /// - binary operators:
    ///     - `*`
    ///     - `div`
    ///     - `lexer`
    fn parse_binary_op2(&mut self) -> Result<Expr, PositionedParseError> {
        let mut lhs_expression = self.parse_unary_op()?;

        loop {
            use KeywordType::*;

            let PositionedToken { token, position } = self.lexer_peek()?.clone();
            match token {
                Token::Keyword(Mult | Div | Mod) => {
                    self.lexer.next();
                    let rhs_expression = self.parse_unary_op()?;
                    lhs_expression = Expr::BinOp {
                        lhs: Box::new(lhs_expression),
                        rhs: Box::new(rhs_expression),
                        op_type: match token {
                            Token::Keyword(Div) => BinaryOpType::Arithmetic(ArithmeticBinaryOpType::Divide),
                            Token::Keyword(Mod) => BinaryOpType::Arithmetic(ArithmeticBinaryOpType::Modulo),
                            Token::Keyword(Mult) => BinaryOpType::Arithmetic(ArithmeticBinaryOpType::Multiply),
                            _ => unreachable!("this should not be reached because this case was checked before"),
                        },
                    }
                }
                Token::Keyword(
                    Plus | Minus | Less | LessOrEqual | More | MoreOrEqual | EqualSign | NotEq | And
                    | Or | Then | Assign | Semicolon | End | ParentClose | Do | To | DownTo
                    | RightSquareBracket | Comma | Else
                ) => break,
                _ => return Err(PositionedParseError::unexpected_token(position, &token, &["*", "div", "mod", "..."]))
            }
        }

        Ok(lhs_expression)
    }

    /// expr6:
    /// - unary operators:
    ///   - `-`
    ///   - `+`
    ///   - `!`
    fn parse_unary_op(&mut self) -> Result<Expr, PositionedParseError> {
        enum Unary { Minus, Plus, Neg }
        let mut unary_operations: Vec<Unary> = Vec::new();

        loop {
            let PositionedToken { token, position } = self.lexer_peek()?;
            match token {
                Token::Keyword(KeywordType::Minus) => unary_operations.push(Unary::Minus),
                Token::Keyword(KeywordType::Plus) => unary_operations.push(Unary::Plus),
                Token::Keyword(KeywordType::LogicalNot) => unary_operations.push(Unary::Neg),

                Token::IntLiteral(_) | Token::Identifier(_) | Token::Keyword(KeywordType::ParentOpen | KeywordType::ReadLn) => break,
                _ => {
                    return Err(PositionedParseError::unexpected_token(
                        *position,
                        &token,
                        &["unary operator (-, +, !)", "int literal", "variable identifier", "( expression )"]));
                }
            }
            self.lexer.next();
        }

        let mut expression = self.parse_suffix_op()?;
        for op in unary_operations.iter().rev() {
            expression = Expr::UnaryOp {
                arg: Box::new(expression),
                op_type: match op {
                    Unary::Minus => UnaryOpType::UnaryMinus,
                    Unary::Plus => UnaryOpType::UnaryPlus,
                    Unary::Neg => UnaryOpType::Negation,
                },
            };
        }
        Ok(expression)
    }


    /// expr7:
    /// - array access
    /// - function call
    fn parse_suffix_op(&mut self) -> Result<Expr, PositionedParseError> {
        let atom = self.parse_atom()?;
        self.parse_rest_of_suffix_op(atom)
    }
    fn parse_rest_of_suffix_op(&mut self, atom: Expr) -> Result<Expr, PositionedParseError> {
        use KeywordType::*;
        let positioned_token = self.lexer_peek()?;
        let position = positioned_token.position;
        match &positioned_token.token {
            Token::Keyword(ParentOpen) => {
                match atom {
                    Expr::ReadLn(_) => {
                        self.match_keyword(ParentOpen)?;
                        if self.lexer_peek()?.token == Token::Keyword(ParentClose) {
                            self.lexer.next();
                            return Ok(Expr::ReadLn(None));
                        };

                        let arg = self.parse_expr()?
                            .try_into()
                            .map_err(|_| PositionedParseError::new("", position))?;
                        self.match_keyword(ParentClose)?;
                        Ok(Expr::ReadLn(Some(arg)))
                    }
                    Expr::Var(id) => {
                        self.match_keyword(ParentOpen)?;
                        let args = self.parse_function_args()?;
                        self.match_keyword(ParentClose)?;
                        Ok(Expr::FunctionCall { func_id: id, args })
                    }
                    _ => Err(PositionedParseError::new(
                        format!("\"{atom}\" cannot be called as a function").as_str(),
                        position)
                    )
                }
            }
            Token::Keyword(LeftSquareBracket) => {
                if let Expr::Var(id) = atom {
                    self.match_keyword(LeftSquareBracket)?;
                    let index = self.parse_expr()?;
                    self.match_keyword(RightSquareBracket)?;
                    Ok(Expr::ArrayAccess { array_id: id, index: Box::new(index) })
                } else {
                    Err(PositionedParseError::new(
                        format!("\"{atom}\" cannot have [..] as an array").as_str(),
                        positioned_token.position)
                    )
                }
            }
            Token::Keyword(
                Mult | Div | Mod | Plus | Minus | Less | More | LessOrEqual | MoreOrEqual | EqualSign
                | NotEq | And | Or | Then | Assign | Semicolon | End | ParentClose | Do
                | To | DownTo | RightSquareBracket | Comma | Else) => Ok(atom),
            _ => Err(PositionedParseError::unexpected_token(
                positioned_token.position,
                &positioned_token.token,
                &["(", ")", "[", "]", "binary operation", "to", "downto", "assign", "expression ..."]))
        }
    }
    fn parse_function_args(&mut self) -> Result<Vec<Expr>, PositionedParseError> {
        let mut args = Vec::new();

        loop {
            if let PositionedToken {
                token: Token::Keyword(KeywordType::ParentClose),
                position: _
            } = self.lexer_peek()? {
                break;
            }

            if !args.is_empty() {
                self.match_keyword(KeywordType::Comma)?;
            }

            args.push(self.parse_expr()
                .map_err(
                    |PositionedParseError { error: e, position }| {
                        PositionedParseError::new(format!(
                            "Expected expression. Error while parsing the said expression: {e}"
                        ).as_str(), position)
                    }
                )?
            );
        }

        Ok(args)
    }

    /// atom:
    /// - variable, number, whole expression in '(...)'
    fn parse_atom(&mut self) -> Result<Expr, PositionedParseError> {
        let positioned_token = self.lexer_peek()?.clone();
        match positioned_token.token {
            Token::IntLiteral(value) => {
                self.lexer.next();
                Ok(Expr::Number(value))
            }
            Token::Keyword(KeywordType::ReadLn) => {
                self.lexer.next();
                Ok(Expr::ReadLn(None))
            }
            Token::Identifier(name) => {
                self.lexer.next();
                Ok(Expr::Var(name))
            }
            Token::Keyword(KeywordType::ParentOpen) => {
                self.match_keyword(KeywordType::ParentOpen)?;
                let expression = self.parse_expr()?;
                self.match_keyword(KeywordType::ParentClose)?;
                Ok(expression)
            }
            _ => Err(PositionedParseError::unexpected_token(
                positioned_token.position,
                &positioned_token.token,
                &["int literal", "identifier"]))
        }
    }

    /// Tries to match the next token as an int literal
    /// or Keyword(Minus) and int literal.
    pub(super) fn match_signed_int_literal(&mut self) -> Result<i64, PositionedParseError> {
        let PositionedToken { token, position: _ } = self.lexer_peek()?;
        let is_negative = Token::Keyword(KeywordType::Minus) == *token;
        if is_negative { self.lexer.next(); }

        let PositionedToken { token, position } = self.lexer_peek()?;
        if let Token::IntLiteral(value) = token.clone() {
            self.lexer.next();
            let value = value as i64;
            return Ok(if is_negative { -value } else { value });
        }
        Err(PositionedParseError::unexpected_token(*position, &token, &["int literal"]))
    }

    pub(super) fn match_identifier(&mut self) -> Result<String, PositionedParseError> {
        let PositionedToken { token, position } = self.lexer.next().unwrap()?;
        if let Token::Identifier(identifier) = token {
            return Ok(identifier);
        }

        Err(PositionedParseError::unexpected_token(position, &token, &["variable identifier"]))
    }
}
