#!/bin/bash

# colors for pretty printing
GRN='\033[32m'; # green
RED='\033[31m'; # red
BOLD='\033[1m'; # red
CL='\033[0m';

fail() {
  echo -e $2 "${RED}${BOLD}[FAIL] $1 ${CL}"
}

info() {
  echo -e $2 "${GRN}${BOLD}[INFO] ${CL}$1"
}

MILA_SCRIPT="mila";
TESTS_DIR='tests';
TMP="test-output";
COMPILED_MILA_NAME="compiled-mila";

info "Starting tests"
cd "${TESTS_DIR}"

for dir in *
do
  echo "--------------------------------------------"
  info "Testing the program ${GRN}${dir}${CL}"

  cd ..
  CURRENT_TEST_DIR="${TESTS_DIR}/${dir}"
  CURRENT_MILA_CODE="${CURRENT_TEST_DIR}/${dir}.mila";

  CURRENT_TMP_DIR="${CURRENT_TEST_DIR}/${TMP}"
  rm    -fr "${CURRENT_TMP_DIR}" ;
  mkdir     "${CURRENT_TMP_DIR}";

  if ! bash "${MILA_SCRIPT}" "${CURRENT_MILA_CODE}" 2>"${CURRENT_TMP_DIR}/compilation.log" ; then
    fail "Compilation of '${CURRENT_MILA_CODE}' failed - skipping this test entirely"
    cd "${TESTS_DIR}"
    continue
  fi

  cp "${COMPILED_MILA_NAME}" "${CURRENT_TMP_DIR}"
  cd "${CURRENT_TMP_DIR}"

  for out_file in ../*.out
  do
    [ -e "${out_file}" ] || continue
    test=${out_file%.out}
    name=${test##*/}
    info "Test '${name}': " -n

    if ! ./"${COMPILED_MILA_NAME}" < "${test}.in" 1> "${name}.my.out" 2>"${name}.my.err" ; then
      echo -e "${RED}${BOLD}Failed - failed return code${CL}"
      continue
    fi

    if ! diff -Z -b "${test}.out" "${name}.my.out" >"${name}.diff.log" ; then
      echo -e "${RED}${BOLD}Failed - the program ended with incorrect output${CL}"
    else
      echo -e "${GRN}Ok${CL} :)"
    fi
  done

  cd ../..
done


