#  the program structure
Start   -> program Id ; FunctionDeclarations Block .




# Function declarations
FunctionDeclarations  -> 
FunctionDeclarations  ->  FunctionDeclaration FunctionDeclarations
FunctionDeclaration   ->  FunctionHeader FunctionDetails
FunctionDetails       ->  forward ;
FunctionDetails       ->  Block ;
#
FunctionHeader      ->  function  Id ( FuncArgsDecl ) : Type ;
FunctionHeader      ->  procedure Id ( FuncArgsDecl ) ;
FuncArgsDecl        ->
FuncArgsDecl        ->  IdList : Type RestOfFuncArgsDecl
#
RestOfFuncArgsDecl  ->
RestOfFuncArgsDecl  ->  ; IdList : Type RestOfFuncArgsDecl















# "begin ... end" block
# DeclarationList for local variables
Block    ->  DeclarationList begin StmList end

## List of statements
StmList       ->
StmList       -> Stm RestOfStmList
RestOfStmList ->
RestOfStmList -> ; OptStm RestOfStmList
OptStm ->
OptStm -> Stm







# DeclarationList -> VarBlock ... ; ConstBlock ... ; VarBlock ... ; ConstBlock ... ;
DeclarationList ->
DeclarationList -> VarBlock DeclarationList
DeclarationList -> ConstBlock DeclarationList

# Block for declaring variables via the 'var' keyword
VarBlock     -> var VarDecl VarDeclList
VarDecl      -> IdList : Type ;
#
VarDeclList  ->
VarDeclList  -> VarDecl VarDeclList
#
IdList       -> Id RestOfIdList
RestOfIdList ->
RestOfIdList -> , Id RestOfIdList
#
Type         -> integer
Type         -> array [ Num .. Num ] of integer


# Block for declaring constants via the 'const' keyword
ConstBlock   -> const ConstDecl ConstDeclList
ConstDecl    -> Id = Num ;
#
ConstDeclList ->
ConstDeclList -> ConstDecl ConstDeclList




## statements
Stm -> NonIfStm

NonIfStm -> Block 

NonIfStm     ->    Ex RestOfStmEx
# assignement -  NOTE: right 'Ex' has to be assignable
RestOfStmEx  ->    := Ex
# this should write an error 
RestOfStmEx  ->

# Single instructions
NonIfStm  ->  exit
NonIfStm  ->  writeln ( Printable )
#  NOTE: right 'Ex' has to be assignable
NonIfStm  ->  readln  ( Ex )

# Block Or Statement
BlockOrStm ->    Block
#BlockOrStm ->    Stm

# If
Stm        ->  if Ex then IfBody OptElse
IfBody     ->  Block
#IfBody     ->  NonIfStm
#
OptElse    ->  else BlockOrStm
OptElse    ->

# While
NonIfStm   ->  while Ex do BlockOrStm

# For
NonIfStm   ->  for Id := Ex ForDir Ex do BlockOrStm
ForDir     ->  to
ForDir     ->  downto










# stuff that can be printed
Printable -> Ex
Printable -> StringLiteral







## expressions
### note: expressions must store info whether they are 'assignable'

    Ex    ->   Ex1 RestEx 
RestEx    ->   ||  Ex 
RestEx    ->

    Ex1   ->   Ex2 RestEx1
RestEx1   ->   &&  Ex1
RestEx1   ->

    Ex2   ->   Ex3 RestEx2
RestEx2   ->   =   Ex3
RestEx2   ->   <>  Ex3
RestEx2   ->

    Ex3   ->   Ex4 RestEx3
RestEx3   ->   <   Ex4
RestEx3   ->   >   Ex4
RestEx3   ->   >=  Ex4
RestEx3   ->   <=  Ex4
RestEx3   ->

    Ex4   ->   Ex5 RestEx4
RestEx4   ->   +   Ex4
RestEx4   ->   -   Ex4
RestEx4   ->

    Ex5   ->   Ex6 RestEx5
RestEx5   ->   *   Ex5
RestEx5   ->   div Ex5
RestEx5   ->   mod Ex5
RestEx5   ->

    Ex6   ->   Ex7
Ex6       ->   -    Ex6
Ex6       ->   +    Ex6
Ex6       ->   !    Ex6

# function call
    Ex7   ->   Atom RestEx7
RestEx7   ->   ( FuncArgs )
RestEx7   ->   [ Ex ]
RestEx7   ->
#
FuncArgs       ->
FuncArgs       ->    Ex RestOfFuncArgs
RestOfFuncArgs ->
RestOfFuncArgs ->    , Ex RestOfFuncArgs


Atom      ->   Num
Atom      ->   Id
Atom      ->   ( Ex )

Num -> 0
Num -> 1
Num -> 2
Num -> 3

# identifier
Id -> a
Id -> b
Id -> bruh
Id -> fce


StringLiteral -> "bruh"

